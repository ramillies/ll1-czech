\section{Jednorozměrný pohyb} \label{p11}

Jednorozměrný pohyb je takový, který má jen jeden stupeň volnosti. Nejobecnější tvar lagrangiánu takové soustavy ve stacionárním poli je tento:
\begin{equation} \label{11.1}
	\lagr = \half a(q) \dot q^2 - U(q),
\end{equation}
přičemž $a(q)$ je nějaká funkce zobecněné souřadnice $q$. Je-li to souřadnice kartézská, pak dostaneme (nazvěme tuto souřadnici $x$):
\begin{equation} \label{11.2}
	\lagr = \half m \dot x^2 - U(x).
\end{equation}

Pohybové rovnice odpovídající těmto lagrangiánům se dají integrovat zcela obecně, aniž by bylo nutno se jimi přímo zaobírat, neboť můžeme vyjít z toho, co známe o integrálech pohybu. Použijeme zákon zachování energie, která má pro lagrangián \eqref{11.2} tvar
\[ E = \half m \dot x^2 + U(x). \]
To je diferenciální rovnice prvního řádu, která se dá integrovat separací proměnných. Po menší úpravě obdržíme
\[ \deriv x t = \sqrt{\frac 2 m [E - U(x)]}, \]
z čehož dostaneme
\begin{equation} \label{11.3}
	t = \sqrt{\frac m 2} \int \frac{\D x}{\sqrt{E - U(x)}} + \text{const.}
\end{equation}
Roli dvou integračních konstant, které by se objevily při řešení Lagrangeovy rovnice (druhého řádu), zde má celková energie $E$ a integrační konstanta const.

Jelikož kinetická energie musí být nutně nezáporná, je potřeba, aby byla při pohybu celková energie vždy větší než energie potenciální; pohyb se tedy může odehrávat jen v těch částech prostoru, kde platí $U(x) < E$.

Řekněme například, že by měla potenciální energie tvar vyobrazený na obrázku 6. Uděláme-li v jejím grafu vodorovnou přímku, která odpovídá celkové energii částice, hned uvidíme, kde se může pohyb odehrávat. Například na onom obrázku je to jen v oblasti mezi A a B a pak v oblasti napravo od C.

\wfig{6}{6cm}

Body, v nichž je potenciální energie rovna celkové energii
\begin{equation} \label{11.4}
	U(x) = E,
\end{equation}
určují právě hranice oblastí, v nichž je pohyb možný. Říká se jim \emph{body zastavení}, protože se v nich nutně musí rychlost rovnat nule. {\footnotesize [ Česky se jim říká, pokud moje znalosti stačí, spíš \emph{body obratu}, ale vůbec nejčastěji se jim neříká nijak. ] } Je-li oblast pohybu ohraničena dvěma takovými body, pak se celý pohyb odehrává v omezené oblasti prostoru; takovému se říká \emph{finitní}. Naopak, pokud je některá oblast ohraničena jen z jedné strany nebo vůbec, pak je pohyb \emph{infinitní} --- částice nakonec uteče do nekonečna.

Jednorozměrný finitní pohyb je nutně kmitavý --- částice opakuje stále týž pohyb, periodicky se přemisťujíc mezi oběma hranicemi ve své oblasti (takové se říká také \emph{potenciálová jáma} --- i když hlavně v kvantové mechanice). Přitom v souladu s tím, že ke každému pohybu je možný vždy pohyb přesně opačný, trvá pohyb z jedné strany na druhou stejně jako z druhé strany na první. Tento čas je podle \eqref{11.3} roven
\begin{equation} \label{11.5}
	T(E) = \sqrt{2m} \int\limits_{x_1(E)}^{x_2(E)} \frac{\D x}{\sqrt{E - U(x)}},
\end{equation}
přičemž meze $x_1, x_2$ jsou vlastně kořeny rovnice \eqref{11.4} pro danou hodnotu energie $E$. Tento vzorec tedy určuje periodu pohybu v závislosti na celkové energii částice.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u11.1} \textbf{1.} Určit periodu kmitání rovinného matematického kyvadla (bod s hmotností $m$ na konci nehmotné tyčky o délce $l$ v tíhovém poli) v závislosti na jeho amplitudě.

\textbf{Řešení.} Energie kyvadla je
\[ E = \half m l^2 \dot \phi^2 - mgl \cos \phi = - mgl \cos \phi_0, \]
kde $\phi$ je úhel, který tyčka svírá se svislicí a $\phi_0$ je maximální výkmit kyvadla. Periodu vypočteme jako čtyřnásobek času, který je potřebný k projití úhlů od 0 do $\phi_0$. Nalezneme, že
\[ T = 4\ \sqrt{\frac{l}{2g}} \int\limits_0^{\phi_0} \frac{\D \phi}{\sqrt{\cos \phi - \cos \phi_0}} = 2\ \sqrt{\frac l g} \int\limits_0^{\phi_0} \frac{\D \phi}{\sin^2 \frac{\phi_0}{2} - \sin^2 \frac \phi 2}. \]
Substitucí $\sin z = \frac{\sin \half \phi}{\sin \half \phi_0}$ se tento integrál převede na tvar
\[ T = 4\ \sqrt{\frac l g} \mathcal K \left(\sin \frac{\phi_0}{2} \right), \]
kde
\[ \mathcal K(k) = \int\limits_0^{\pi/2} \frac{\D t}{\sqrt{1 - k^2 \sin^2 t}} \]
je takzvaný \emph{úplný eliptický integrál prvého druhu}. {\footnotesize [ Ten se nedá vyjádřit pomocí elementárních funkcí a je třeba ho pro danou hodnotu najít v tabulkách --- i když v dnešní době už daleko spíš spočítat počítačem. ] } Při $\sin \phi_0/2 \ll 1$ (tedy při malých kmitech) se může funkce pod integrálem v $\mathcal K$ rozvést v řadu, jejíž integrací se následně obdrží rozvoj tvaru
\[ T = 2\pi\ \sqrt{\frac l g} \left( 1 + \frac{1}{16} \phi_0^2 + \ldots \right).\]
První člen tohoto rozkladu odpovídá známé elementární formulce.

\phantomsection \label{u11.2} \textbf{2.} Stanovit periodu kmitů v závislosti na energii při pohybu částice o hmotnosti $m$ v polích s potenciální energií ve tvaru:
\begin{ce}
\item $U = A |x|^n$;
\item $U = -U_0 / \ch^2 ax;\quad -U_0 < E < 0$;
\item $U = U_0 \tg^2 ax$.
\end{ce}

\textbf{Odpověď.}
\begin{ce}
\item \[ T = 2\ \sqrt{2m} \int\limits_0^{(E/A)^{1/n}} \frac{\D x}{\sqrt{E - Ax^n}} = \frac{2\ \sqrt{2m} E^{\frac 1 n - \frac 1 2}}{A^{1/n}} \int\limits_0^1 \frac{\D y}{\sqrt{1 - y^n}}. \]
Substitucí $u = y^n$ se tento integrál převede na takzvaný Eulerův beta-integrál, který je možno vyčíslit pomocí gamma-funkcí:
	\[ T = \frac{2\ \sqrt{2\pi m} \Gamma\left(\frac 1 n \right)}{n A^{1/n} \Gamma\left(\frac 1 n + \frac 1 2\right)} E^{\frac 1 n - \frac 1 2}. \]
	Závislost $T$ na $E$ přitom splňuje zákony mechanické podobnosti \eqref{10.2} a \eqref{10.3}.

\item \[ T = \pi\ \sqrt{2m} / \alpha\ \sqrt{|E|}. \]
\item \[ T = \pi\ \sqrt{2m} / \alpha\ \sqrt{E + U_0}. \]
\end{ce}

\section{Určení potenciální energie podle periody kmitů} \label{p12}

Nyní vyšetříme, jak dobře je možné stanovit tvar potenciální energie $U(x)$ pole, v němž částice vykonává kmitavý pohyb, když už známe závislost periody $T$ od energie částice $E$. Z pohledu matematiky jde prostě o řešení integrální rovnice \eqref{11.5}, kde $U(x)$ je neznámá a $T(E)$ známá funkce.

\rightwfig{7}{5cm}{0.33}

Přitom budeme od počátku předpokládat, že hledaná funkce $U(x)$ má ve vyšetřované oblasti prostoru jedno minimum, a řešením (ani řešitelností) rovnice pro takové potenciální energie, které to nesplňují, se nebudeme vůbec zabývat. Pro pohodlí označíme souřadnici minima jako 0 a potenciální energii v něm položíme též rovnu nule, jako na obr. 7.

Transformujme integrál \eqref{11.5} tak, že v něm budeme souřadnici $x$ považovat za funkci $U$. Taková funkce $x(U)$ je zřejmě dvojznačná, protože každé hodnoty potenciální energie $U$ částice nabývá ve dvou různých bodech $x$. Z toho důvodu přejde integrál \eqref{11.5}, poté, co v něm zaměníme $\D x$ za $\deriv x U \D U$, v součet dvou integrálů: jednoho od $x_1$ do 0, druhého od 0 do $x_2$. V těchto oblastích tedy budeme psát závislost $x$ na $U$ jako $x_1(U)$ a $x_2(U)$.

Integračními mezemi podle $U$ budou zřejmě 0 a $E$. Z toho dostaneme
\[ T(E) = \sqrt{2m} \left[ \int\limits_0^E \deriv{x_2(U)}{U} \frac{\D u}{\sqrt{E - U}} + \int\limits_E^0 \deriv{x_1(U)}{U} \frac{\D u}{\sqrt{E - U}} \right] = \sqrt {2m} \int\limits_0^E \deriv{x_2 - x_1}{U} \frac{\D U}{\sqrt{E - U}}. \]

Nyní zavedeme nový parametr $\alpha$ a obě strany této rovnice vydělíme $\sqrt{\alpha - E}$. Učinivše, integrujme podle $E$ od 0 do $\alpha$:
\[ \int\limits_0^\alpha \frac{T(E) \D E}{\sqrt{\alpha - E}} = \sqrt{2m} \int\limits_0^\alpha \int\limits_0^E \deriv{x_2 - x_1}{U} \frac{\D U \D E}{\sqrt{(\alpha - E)(E-U)}}. \]
V tom zaměňme pořadí integrace:
\[ \int\limits_0^\alpha \frac{T(E) \D E}{\sqrt{\alpha - E}} = \sqrt{2m} \int\limits_0^\alpha \deriv{x_2 - x_1}{U} \D U \int\limits_U^\alpha \frac{\D E}{\sqrt{(\alpha - E)(E-U)}}. \]

Integrál podle $E$ se dá vyčíslit pomocí elementárních funkcí, a když se to udělá, zjistí se, že je roven $\pi$. Integrace podle $\D U$ je pak triviální a dostaneme
\[ \int\limits_0^\alpha \frac{T(E) \D E}{\sqrt{\alpha - E}} = \sqrt{2m} \pi [ x_2(\alpha) - x_1(\alpha) ]\]
(využili jsme přitom, že $x_1(0) = x_2(0) = 0$). Dosadíme nyní za parametr $\alpha$ $U$ a nalezneme
\begin{equation} \label{12.1}
x_2(U) - x_1(U) = \frac{1}{\sqrt{2m} \pi} \int\limits_0^U \frac{T(E) \D E}{\sqrt{U - E}}. 
\end{equation}

Podle známé funkce $T(E)$ tedy dostaneme rozdíl $x_2(U) - x_1(U)$. Samotné funkce $x_2$ a $x_1$ tím ovšem určeny nejsou; existuje tedy ne jedna, ale nekonečné množství možných křivek $U(x)$, které vedou zadané závislosti periody na energii a které se od sebe navzájem liší takovými deformacemi, které nemění rozdíly obou $x$, v nichž je potenciální energie rovna $U$.

Mnohoznačnosti se zbavíme, když budeme požadovat, aby byla potenciální energie symetrická vůči minimu, tedy aby platilo
\[ x_2(U) = -x_1(U) \equiv x(U). \]
Pak můžeme přepsat \eqref{12.1} tak, aby byl výsledek jednoznačný:
\begin{equation} \label{12.2}
	x(U) = \frac{1}{2\pi \sqrt{2m}} \int\limits_0^U \frac{T(E) \D E}{\sqrt{U - E}}.
\end{equation}

\section{Redukovaná hmotnost} \label{p13}

Zabývejme se nyní pohybem soustav, které obsahují jen dvě vzájemně interagující částice. Tuto úlohu, takzvaný \emph{problém dvou těles}, je možno řešit v úplné obecnosti, což i nyní provedeme.

Než však přistoupíme k tomuto řešení, provedeme jeden přípravný krok, který nám značně usnadní práci, neboť umožní rozložit pohyb soustavy na pohyb těžiště a pohyb obou částic vzhledem k němu.

Potenciální energie interagujících těles může záviset jen na vzdálenosti mezi nimi, tedy na velikosti rozdílu jejich polových vektorů. Lagrangián této soustavy je tedy roven
\begin{equation} \label{13.1}
	\lagr = \half m_1 \dot{\vv r_1^2} + \half m_2 \dot{\vv r_2^2} - U(|\vv r_1 - \vv r_2|).
\end{equation}
Zavedeme označení pro vzájemnou polohu obou bodů:
\[ \vv r = \vv r_1 - \vv r_2 \]
a umístíme počátek souřadnic do těžiště soustavy, což dá
\[ m_1 \vv r_1 + m_2 \vv r_2 = \vv 0. \]
Řešením soustavy dvou předchozích rovnic dostaneme vztahy
\begin{align} \label{13.2}
	\vv r_1 &= \frac{m_2}{m_1 + m_2} \vv r,&
	\vv r_2 &= - \frac{m_1}{m_1 + m_2} \vv r.
\end{align}
Dosadíme-li to do \eqref{13.1}, obdržíme
\begin{equation} \label{13.3}
	\lagr = \half \mu \dot{\vv r^2} - U(r),
\end{equation}
přičemž jsme zavedli označení
\begin{equation} \label{13.4}
\mu = \frac{m_1m_2}{m_1 + m_2} = \frac{1}{\frac{1}{m_1} + \frac{1}{m_2}}; 
\end{equation}
tato veličina se nazývá \emph{redukovanou hmotností} soustavy. Funkce \eqref{13.3} je formálně zcela shodná s lagrangiánem jednoho hmotného bodu s hmotností $m$, která se pohybuje ve vnějším poli $U(r)$, symetrickém vůči nepohyblivému počátku souřadné soustavy.

Tímto způsobem se tedy redukovala úloha o dvou tělesech na řešení úlohy o pohybu jednoho bodu v zadaném vnějším poli $U(r)$. Jakmile obdržíme řešení pro $\vv r$, trajektorie $\vv r_1$ a $\vv r_2$ (vzhledem k těžišti soustavy) vypočteme podle \eqref{13.2}.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u13.1} Soustava obsahuje jednu částici s hmotností $M$ a $n$ částic se stejnou hmotností $m$. Vyloučit z pohybových rovnic pohyb těžiště a převést na úlohu o pohybu $n$ částic.

\textbf{Řešení.} Nechť je $\vv R$ polohový vektor částice $M$ a $\vv R_i$ polohové vektory částic s hmotností $m$. Zavedeme vzdálenosti mezi částicí $M$ a $m$:
\[ \vv r_a = \vv R_a - \vv R \]
a umístíme počátek souřadnic do těžiště:
\[ M\vv R + m \sum_a \vv R_a = 0. \]
Z těchto rovnic plyne
\begin{align*}
	\vv R &= - \frac{m}{M + nm} \sum_a \vv r_a,&
	\vv R_a = \vv R + \vv r_a.
\end{align*}
To dosadíme do lagrangiánu:
\[ \lagr = \half M \dot{\vv R^2} + \half m \sum_a \dot{\vv R_a}^2 - U, \]
upravíme a dostaneme
\[ \lagr = \half m \sum_a \vv v_a^2 - \frac{m^2}{2(M + nm)} \left( \sum_a \vv v_a \right)^2 - U, \]
přičemž $\vv v_a \equiv \dot{\vv r_a}$.

Potenciální energie závisí pouze na vzdálenostech mezi částicemi a může být proto psána jako funkce vektorů $\vv r_a$.

\normalsize

\section{Pohyb v centrálním poli} \label{p14}

Úlohu o dvou tělesech se nám podařilo převést na řešení pohybu jedné částice, ale pořád ještě neumíme v obecnosti vypočíst výslednou úlohu o pohybu hmotného bodu, jehož potenciální energie závisí pouze na vzdálenosti od jednoho nehybného středu (v tomto případě se často mluví o \emph{pohybu v centrálním poli}). Nyní tedy tuto úlohu vyřešíme.

Síla
\[ \vv F = - \pderiv{U(r)}{\vv r} = - \deriv U r \frac{\vv r}{r}, \]
která na částici působí, závisí svou velikostí pouze na vzdálenosti částice od středu; její směr je pak týž jako směr spojnice bodu se středem.

Jak už jsme ukázali v § 9, při pohybu v centrálním poli se musí zachovávat moment hybnosti soustavy vzhledem ke středu pole. Pro jednu částici je tento moment roven
\[ \vv M= \vv r \times \vv p. \]
Jelikož vektory $\vv M$ a $\vv r$ jsou navzájem kolmé, značí konstantnost $\vv M$, že se pohyb odehrává pouze v jedné rovině, a to té, která je na $\vv M$ kolmá.

Trajektorie pohybu částice v centrálním poli tedy leží v jedné rovině. Zavedeme-li polární souřadnice $r, \phi$, dostaneme lagrangián ve tvaru (viz \eqref{4.5})
\begin{equation} \label{14.1}
	\lagr = \half m (\dot r^2 + r^2 \dot \phi^2) - U(r).
\end{equation}

Souřadnice $\phi$ (bez tečky) se vůbec v lagrangiánu nevyskytuje, takže je $\phi$ cyklická (viz \hyperref[7.7]{poznámku na konci § 7}). Z toho důvodu se zobecněná hybnost $p_\phi$:
\[ p_\phi = \pderiv{\lagr}{\dot \phi} = mr^2 \dot \phi \]
zachovává. (V tomto případě řečená veličina odpovídá momentu $M = M_z$ (viz \eqref{9.6}), takže jsme nedostali nic nového než opět náš zákon zachování momentu hybnosti
\begin{equation} \label{14.2}
	M = mr^2 \dot \phi = \text{const.}
\end{equation}
.) 

\rightfig 8

Poznamenejme, že pro rovinné pohyby jedné částice v centrálním poli má tento zákon velmi jednoduchou geometrickou interpretaci. Výraz $\half r^2 \D \phi$ představuje totiž plochu opsanou průvodičem bodu na křivce při infinitesimálním posunu tohoto bodu (je to vidět z obr. 8). Označíme-li to jako $\D f$, je možno moment hybnosti psát ve tvaru
\begin{equation} \label{14.3}
	M = 2m \dot f, 
\end{equation}
kde se časová derivace $\dot f$ nazývá \emph{plošnou rychlostí}. Zachování momentu hybnosti je tedy ekvivalentní konstantnosti plošné rychlosti. Jestliže to formulujeme slovy \clqq Plocha opsaná za jednotku času průvodičem bodu je konstantní,\crqq\ dostaneme známé vyjádření \emph{druhého Keplerova zákona} \footnote{V analogii s integrály pohybu se někdy momentu hybnosti částice z tohoto důvodu říká \emph{integrál plochy}.}

Nejsnáze se úplného řešení úlohy o pohybu v centrálním poli dobereme, když vyjdeme ze zákonů zachování energie a momentu hybnosti, aniž bychom řešili vlastní pohybové rovnice. Vyjádříme podle \eqref{14.2} $\dot \phi$ pomocí $M$ a dosadíme to do výrazu pro energii:
\begin{equation} \label{14.4}
	E = \half m(\dot r^2 + r^2 \dot \phi^2) + U(r) = \half m \dot r^2 + \frac{M^2}{2mr^2} + U(r).
\end{equation}
Z toho dostaneme
\begin{equation} \label{14.5}
	\dot r = \deriv r t = \sqrt{\frac 2 m [E - U(r)] - \left(\frac{M}{mr}\right)^2},
\end{equation}
nebo po separaci proměnných:
\begin{equation} \label{14.6}
	\D t = \frac{\D r}{\sqrt{\frac 2 m [E - U(r)] - \left(\frac{M}{mr}\right)^2}}.
\end{equation}
Přepišme dále \eqref{14.2} do tvaru
\[ \D \phi = \frac{M}{mr^2} \D t, \]
načež dosadíme z \eqref{14.6} $\D t$ a integrujeme:
\begin{equation} \label{14.7}
	\phi = \int \frac{\frac{M}{r^2} \D r}{\sqrt{2m[E - U(r)] - \left(\frac M r\right)^2}} + \text{const.}
\end{equation}

Vztahy \eqref{14.6} a \eqref{14.7} obecně řeší zadanou úlohu. Druhá z těchto rovnic určuje vztah mezi $r$ a $\phi$, tedy rovnici trajektorií. Vztah \eqref{14.6} určuje zase vzdálenost $r$ od středu jako funkci času. Poznamenejme, že úhel $\phi$ se vždy mění s časem monotonně: z \eqref{14.2} vidíme, že $\dot \phi$ nikdy nemění znamení.

Výraz \eqref{14.4} ukazuje, že radiální část pohybu je možno brát jako jednorozměrný pohyb v poli s \clqq efektivní\crqq\ potenciální energií
\begin{equation} \label{14.8}
	U_e = U(r) + \frac{M^2}{2mr^2}.
\end{equation}
Veličina $M^2/2mr^2$ se někdy nazývá \emph{dostředivou energií}. {\footnotesize [ Já jsem tohle slovní spojení nikdy neslyšel, ale co čtu, to překládám. ] } Hodnoty $r$, při kterých platí rovnice
\begin{equation} \label{14.9}
	U(r) + \frac{M^2}{2mr^2} = E,
\end{equation}
jsou vlastně hranicemi oblasti pohybu. V těchto bodech se musí radiální rychlost $\dot r$ rovnat nule. To ale neznamená úplné zastavení částice, protože rychlost úhlová $\dot \phi$ nulová nebude. Rovnost $\dot r = 0$ pouze označuje \clqq bod obratu\crqq\ tajektorie, ve kterém se vzdálenost $r$ začne zvětšovat, když se původně zmenšovala (nebo obráceně).

Je-li oblast přípustného pohybu ohraničena pouze podmínkou $r \leq r_\text{min}$, pak je pohyb částice infinitní: přijde z nekonečna a zase se do nekonečna nakonec odebere.

Má-li tato oblast ovšem dvě hranice $r_\text{min}$ a $r_\text{max}$, je pohyb finitní a trajektorie leží úplně v mezikruží mezi kružnicemi o těchto dvou poloměrech. To ovšem nutně neznamená, že by trajektorie byla uzavřenou křivkou. Za čas, během kterého se $r$ změní z $r_\text{max}$ na $r_\text{min}$ a zase zpět na $r_\text{max}$, se totiž bod otočí o úhel $\Delta \phi$, který je podle \eqref{14.7} roven
\begin{equation} \label{14.10}
	\Delta \phi = 2 \int\limits_{r_\text{min}}^{r_\text{max}} \frac{\frac{M}{r^2} \D r}{\sqrt{2m(E-U) - \left(\frac M r \right)^2}}.
\end{equation}

\rightwfig{9}{6cm}{0.35}

Trajektorie bude zřejmě uzavřená pouze v případě, že se bude tento úhel rovnat nějakému racionálnímu násobku $2\pi$, tedy bude mít tvar $\Delta \phi = 2\pi m/n$, kde $m$ a $n$ jsou celá čísla. Za $n$ period tedy bod vykoná právě $m$ úplných oběhů a bude se nacházet přesně v té poloze, v níž před těmito $n$ periodami původně začínal, a trajektorie bude uzavřená.

Jak je ale snad vidět, může se vzhledem k libovolnosti $U(r)$ klidně stát, že $\Delta \phi$ nebude racionálním násobkem $2\pi$, a proto trajektorie nutně uzavřená být nemusí. V takovém případě se nekonečněkrát dotkne obou hraničních kružnic (jako třeba na obr. 9) a za nekonečně dlouhou dobu vyplní celé mezikruží.

Existují pouze dva druhy centrálních polí, v nichž jsou všechny trajektorie finitních pohybů uzavřené. Jsou to pole, v nichž je potenciální energie částice úměrná $1/r$ nebo $r^2$. První z těchto případů podrobně rozebereme v \hyperref[p16]{následujícím paragrafu}, zatímco druhý odpovídá takzvanému prostorovému oscilátoru (viz úlohu \hyperref[u23.3]{3 v paragrafu § 23}).

V bodě obratu se musí u odmocniny v \eqref{14.5} (a současně s tím i v \eqref{14.6} a \eqref{14.7}) změnit znamení. Jestliže budeme úhel $\phi$ měřit od přímky procházející středem pole a bodem obratu, bude trajektorie vůči této přímce osově souměrná: při stejných hodnotách $r$ bude tedy bod procházet vždy dvěma úhly $\pm \phi$. Jestliže tedy začneme od některého z bodů na kružnici $r_\text{max}$ a vezmeme úsek trajektorie až k dalšímu bodu na kružnici $r_\text{min}$, můžeme další úsek z tohoto $r_\text{min}$ do dalšího $r_\text{max}$ sestrojit pouhou osovou souměrností. Totéž platí i pro infinitní pohyby, u nichž je trajektorie osově souměrná vůči přímce procházející středem a jediným bodem obratu, který trajektorie má.

Platí-li $M \neq 0$, roste hodnota dostředivé energie při $r \to 0$ nade všechny meze úměrně $1/r^2$. Z toho vidíme, že pokud není pole přitažlivé, není možné, aby částice pronikla k jeho středu. \clqq Padání\crqq\ částice do středu pole je možno jen v případě, že při $r \to 0$ potenciální energie dostatečně rychle klesá k $-\infty$. Z nerovnosti
\[ \half m \dot r^2 = E -U(r) - \frac{M^2}{2mr^2} > 0 \]
nebo-li
\[ r^2 U(r) + \frac{M^2}{2m} < Er^2 \]
dostáváme po přechodu k limitě $r \to 0$ podmínku
\begin{equation} \label{14.11}
	\lim_{r \to 0} [ r^2 U(r) - Er^2 ] = \lim_{r \to 0} r^2 U(r) < - \frac{M^2}{2m},
\end{equation}
tedy $U(r)$ musí jít k $-\infty$ buď úměrně $-\alpha / r^2$, přičemž $\alpha > M^2/2m$, nebo úměrně $-1/r^n$ s $n > 2$.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u14.1} \textbf{1.} Integrovat rovnice sférického kyvadla: hmotného bodu s hmotností $m$, který se pohybuje v tíhovém poli po kulové ploše s poloměrem $l$.

\textbf{Řešení.} Ve sférických souřadnicích s počátkem ve středu koule a polární osou mířící svisle dolů můžeme napsat lagrangián kyvadla ve tvaru
\[ \lagr = \half ml^2(\dot \theta^2 + \sin^2 \theta \dot \phi^2) + mgl \cos \theta. \]
Souřadnice $\phi$ je cyklická, takže se zachovává zobecněná hybnost $p_\phi$ (rovná zetové složce momentu hybnosti):
\[ ml^2 \sin^2 \theta \dot \phi = M_z = \text{const.} \]

Energie
\[ E = \half ml^2(\dot \theta^2 + \sin^2 \theta \dot \phi^2) - mgl \cos \theta = \half ml^2 \dot \theta^2 + \frac{M_z^2}{2ml^2 \sin^2 \theta} - mgl \cos \theta. \]
Vyjádřením $\dot \theta$ a separací proměnných dostaneme
\[ t = \int \frac{\D \theta}{\sqrt{\frac{2}{ml^2} [E - U_e(\theta)]}}, \]
kde jsme zavedli \clqq efektivní potenciální energii\crqq
\[ U_e(\theta) = \frac{M_z^2}{2ml^2 \sin^2 \theta} - mgl \cos \theta. \]
Pro úhel $\phi$ po použití zákona zachování $M_z$ dostaneme
\[ \phi = \frac{M_z}{\sqrt{2m} l} \int \frac{\D \theta}{\sin^2 \theta\ \sqrt{E - U_e(\theta)}}. \]
Poslední dva integrály se převedou na eliptické integrály po řadě prvého a třetího druhu.

Pohyb je možný jen při $E > U_e$ a hranice oblasti pohybu je tedy dána rovnicí $E = U_e$. To je kubická rovnice pro $\cos \theta$, která má v intervalu od $-1$ do $+1$ dvě řešení. Tato řešení udávají polohu dvou kružnic na sféře, mezi nimiž se musí odehrávat veškerý pohyb.

\phantomsection \label{u14.2} \textbf{2.} Integrovat rovnice pohybu hmotného bodu, který se pohybuje po kuželové ploše s úhlem $2\alpha$ ve vrcholu. Kužel je umístěn v tíhovém poli svisle, vrcholem dolů.

\textbf{Řešení.} Ve sférických souřadnicích s počátkem ve vrcholu kužele a polární osou směřující svisle vzhůru dostaneme lagrangián
\[ \lagr = \half m(\dot r^2 + r^2 \sin^2 \alpha \dot \phi^2) - mgr \cos \alpha. \]
$\phi$ je opět cyklická, takže se zase zachovává
\[ M_z = mr^2 \sin^2 \alpha \dot \phi. \]
Energie:
\[ E = \half m \dot r^2 + \frac{M_z^2}{2mr^2 \sin^2 \alpha} + mgr \cos \alpha. \]
Stejně jako v předchozí úloze najdeme
\begin{align*}
	t &= \int \frac{\D r}{\sqrt{\frac 2 m [E - U_e(r)]}},\\
	\phi &= \frac{M_z}{\sqrt{2m} \sin^2 \alpha} \int \frac{\D r}{\sqrt{E - U_e(r)} r^2},\\
	U_e(r) &= \frac{M_z^2}{2mr^2 \sin^2 \alpha} +mgr \cos \alpha.
\end{align*}

Podmínka $E = U_e(r)$ je při nenulovém $M_z$ kubickou rovnicí pro $r$ se dvěma kladnými kořeny, které určují dvě vodorovné kružnice na povrchu kužele, mezi nimiž jsou obsaženy všechny trajektorie.

\phantomsection \label{u14.3} \textbf{3.} Integrovat rovnice pohybu rovinného kyvadla, jehož závěs se může pohybovat vodorovně, zcela jako v \hyperref[u5.2]{úloze 2 v § 5}.

\textbf{Řešení.} V lagrangiánu z řešení této úlohy vidíme, že je souřadnice $x$ cyklická. Má se tedy zachovávat zobecněná hybnost $P_x$, která je rovna $x$-ové složce úplné hybnosti soustavy:
\[ P_x = (m_1 + m_2) \dot x + m_2 l \dot \phi \cos \phi = \text{const.} \]
Vždy je možno uvažovat, že se soustava jako celek nepohybuje; pak $\text{const.} = 0$ a po integraci předchozí rovnice dostaneme
\[ (m_1 + m_2) x+ m_2 l \sin \phi = \text{const}, \]
což vyjadřuje podmínku pro to, aby se těžiště soustavy nepohybovalo ve vodorovném směru. Po využití první rovnice dostaneme energii ve tvaru
\[ E = \half m_2 l^2 \dot \phi^2 \left(1 - \frac{1}{1 + \frac{m_1}{m_2}} \cos^2 \phi \right) - m_2 gl \cos \phi. \]
Z toho
\[ t = l\ \sqrt{\frac{\half}{1 + \frac{m_1}{m_2}}} \int \sqrt{\frac{m_1 + m_2 \sin^2 \phi}{E + m_2 gl \cos \phi}} \D \phi. \]

Vyjádřivše souřadnice $x_2 = x + l \sin \phi$ a $y_2 = l \cos \phi$ částice $m_2$ pomocí $\phi$, zjistíme, že trajektorií je část elipsy s vodorovnou poloosou rovnou $l/(1+m_2/m_1)$ a svislou poloosou rovnou $l$. Při $m_1 \to \infty$ se dostaneme k běžnému matematickému kyvadlu, které se kýve po části kružnice.

\normalsize

\section{Keplerova úloha} \label{p15}

Velmi důležitým případem centrálních polí jsou taková pole, jejichž potenciální energie je nepřímo úměrná $r$, a síla, která v nich působí, je tedy nepřímo úměrná $r^2$. To je zejména newtonovské gravitační pole a Coulombovo pole elektrostatické. Jak známo, má první z nich charakter výhradně přitažlivý, zatímco druhé může být jak přitažlivé, tak i odpudivé.

\rightfig{10}

Nejdřív rozeberme čistě přitažlivé pole, v němž platí
\begin{equation} \label{15.1}
	U = - \frac C r,
\end{equation}
kde $a$ je kladná konstanta. Graf \clqq efektivní\crqq\ potenciální energie
\begin{equation} \label{15.2}
	U_e = - \frac C r + \frac{M^2}{2mr^2}
\end{equation}
má tvar vypodobený na obrázku 10. Při $r \to 0$ jde k $+\infty$, zatímco při $r \to \infty$ konverguje k nule. Minimum je v $r = M^2/Cm$ a potenciální energie v něm má hodnotu
\begin{equation} \label{15.3}
	\min U_e = - C^2 m / 2M^2.
\end{equation}
Z tohoto grafu je hned vidno, že při $E > 0$ bude pohyb částice infinitní, zatímco při $E < 0$ finitní.

Tvar trajektorie se najde s pomocí obecného vztahu \eqref{14.7}. Jestliže dosadíme $U = - C/r$ a provedeme elementární integraci, dostaneme
\[ \phi = \arccos \frac{\frac M r - \frac{Cma}{M}}{\sqrt{2mE + \left(\frac{Cm}{M}\right)^2}} + \text{const.} \]
Počítejme úhel $\phi$ tak, aby bylo $\text{const.} = 0$, a zaveďme označení
\begin{align} \label{15.4}
	p &= \frac{M^2}{Cm}, &
	e &= \sqrt{1 + \frac{2EM^2}{C^2m}};
\end{align}
tím se podaří vztah pro trajektorii přepsat do tvaru
\begin{equation} \label{15.5}
	r = \frac{p}{1 + e \cos \phi}.
\end{equation}
To je známá polární rovnice kuželosečky s jedním ohniskem v počátku souřadnic. {\footnotesize [ \textbf{Návod k důkazu}. Pro elipsu se tvrzení dostane použitím kosinové věty na trojúhelník s dvěma vrcholy v obou ohniscích a s třetím někde na elipse. Strana mezi ohnisky má délku $2ae$ a součet obou zbylých stran je $2a$, takže druhé dvě strany mají délky $2a-r$ a $r$. Pro ostatní kuželosečky podle stejného vzoru. ] }   $p$ a $e$ jsou její \emph{parametr} a \emph{excentricita}. Počáteční směr, od nějž počítáme $\phi$, jsme zvolili tak, že bod s $\phi=0$ je nejbližší ke středu pole (tomuto bodu se říká \emph{perihelium} dráhy).

\rightfig{11}

V problému dvou těles, která na sebe působí podle \eqref{15.1}, je dráha každé částice zvlášť též kuželosečkou s ohniskem ve společném těžišti.

Z \eqref{15.4} je vidět, že při $E < 0$ je excentricita $e < 1$, dráha je tedy elipsou (viz obr. 11) a pohyb je finitní (jak už jsme určili na začátku tohoto paragrafu). Podle známých formulek analytické geometrie jsou hlavní a vedlejší poloosa po řadě rovny
\begin{align} \label{15.6}
	a &= \frac{p}{1-e^2} = \frac{C}{2|E|},&
	b &= \frac{p}{\sqrt{1-e^2}} = \frac{M}{\sqrt{2m|E|}}.
\end{align}
Nejmenší možná energie má stejnou hodnotu jako \eqref{15.3}. Při tom je $e = 0$; z elipsy se tedy stane kružnice. V \eqref{15.6} přitom vidíme, že hlavní poloosa elipsy závisí pouze na energii částice, ale ne na jejím momentu hybnosti. Nejmenší a největší vzdálenosti od středu pole (tedy jednoho ohniska elipsy) jsou rovny
\begin{align} \label{15.7}
	r_\text{min} &= a(1-e) = \frac{C}{2|E|}\left(1 -\ \sqrt{1 + \frac{2EM^2}{C^2 m}}\right),&
	r_\text{max} &= a(1+e) = \frac{C}{2|E|}\left(1 +\ \sqrt{1 + \frac{2EM^2}{C^2 m}}\right).
\end{align}
Tyto výrazy by nakonec bylo možno získat i přímo řešením rovnice $U_e(r) = E$.

Doba oběhu po eliptické dráze, tedy perioda pohybu $T$, se zjistí snadno podle zákona zachování momentu hybnosti ve tvaru \clqq integrálu plochy\crqq\ \eqref{14.3}. Integrujme tuto rovnost podle času od nuly do $T$ a dostaneme:
\[ 2mf = TM, \]
kde $f$ je plocha obepnutá drahou. Pro elipsu platí $f = \pi a b$, díky čemuž s pomocí \eqref{15.6} nalezneme
\begin{equation} \label{15.8}
	T = 2\pi a^{\sfrac 3 2} \sqrt{m/C} = \pi C\ \sqrt{\frac{m}{2 |E|^3}}.
\end{equation}
Že má být čtverec periody úměrný trojmoci lineárních rozměrů dráhy, to jsme ukázali již v \hyperref[p10]{§ 10}. Poznamenejme ještě, že perioda tím závisí jen na energii částice.

\rightfig{12}

Při $E \geq 0$ je pohyb infinitní. Je-li $E > 0$, pak je excentricita $e > 1$ a trajektorie má tvar hyperboly, která obíhá střed pole (jedno ohnisko), jak je naznačeno na obr. 12. Vzdálenost perihelia od středu
\begin{equation} \label{15.9}
	r_\text{min} = a(e-1),
\end{equation}
kde
\[ a = \frac{C}{2E} = \frac{p}{e^2 - 1} \]
je hlavní osa hyperboly.

V případě $E = 0$ a $e = 1$ se částice pohybuje po parabole, jejíž perihelium je ve vzdálenosti $r_\text{min} = p/2$ od středu. Tento případ nastává, pokud částice počíná svůj pohyb z klidu v nekonečnu.

Závislost souřadnic čásice na čase je možno najít s pomocí obecného vzorce \eqref{14.6}. Snáze ji ale dostaneme v parametrickém tvaru následujícím způsobem:

Nejdříve se věnujme eliptickým drahám. Zavedeme podle \eqref{15.4} a \eqref{15.6} $a$ a $e$ a napíšeme s jejch pomocí integrál \eqref{14.6} takto:
\[ t = \sqrt{\frac{m}{2|E|}} \int \frac{r \D r}{\sqrt{-r^2 + \frac{C}{|E|} r - \frac{M^2}{2m|E|}}} = \sqrt{\frac{ma}{C}} \int \frac{r \D r}{\sqrt{a^2e^2 - (r-a)^2}}. \]
Nabízí se přirozená substituce
\[ r - a = -ae \cos u, \]
s jejíž pomocí převedeme integrál do tvaru
\[ t = \sqrt{\frac{ma^3}{C}} \int (1-e \cos u) \D u = \sqrt{\frac{ma^3}{C}} (u - e \sin u) + \text{const.} \]

{\footnotesize [ Při vymýšlení této substituce je možno uvažovat asi takhle: Chceme zneškodnit odmocninu ve jmenovateli, takže by bylo dobré násilně předělat $(r-a)^2$ na $a^2 e^2 \text{něco}^2$. $ae$ pak půjde před integrál a zůstane $\sqrt{1 - \text{něco}^2}$. Vezmeme tedy sinus nebo kosinus. (Máme samozřejmě více možných substitucí, které se liší užitou goniometrickou funkcí nebo znamením, ale všechny jsou v zásadě stejně dobré.) ] }

Zvolíme čas $t = 0$ tak, aby v něm bylo $u = 0$. Konstanta const. tedy vypadne a my dostaneme pěknou parametrickou závislost vzdálenosti na čase:
\begin{align} \label{15.10}
	r &= a(1 - e \cos u), &
	t &= \sqrt{\frac{ma^3}{C}} (u - e \sin u)
\end{align}
Skrz týž parametr $u$ je možno vyjádřit i kartézské souřadnice částice $x = r \cos \phi$ a $y = r \sin \phi$ (osy $x$ a $y$ odpovídají hlavní a vedlejší poloose elipsy). Z \eqref{15.5} a \eqref{15.10} dostaneme
\[ ex = p-r = a(1-e^2) - a(1-e \cos u) = ae(\cos u - e), \]
a $y$ nalezneme jako $\sqrt{r^2 - x^2}$ (případně se záporným znamením). Nakonec obdržíme
\begin{align} \label{15.11}
	x &= a(\cos u - e), &
	y &= a \sqrt{1 - e^2} \sin u.
\end{align}
Jednomu oběhu po elipse zde odpovídá změna parametru od $0$ do $2 \pi$.

Zcela analogické výrazy se obdrží i pro hyperbolické trajektorie:
\begin{align} \label{15.12}
	r &= a(e \ch u - 1), & t &= \sqrt{\frac{ma^3}{C}} (e \sh u - u), &
	x &= a(e - \ch u), & y &= a \sqrt{e^2 - 1} \sh u,
\end{align}
kde parametr $u$ probíhá od $-\infty$ do $+\infty$.

\rightfig{13}

Ještě vyšetřeme pohyb v odpudivém poli, kde je
\begin{equation} \label{15.13}
	U = C/r
\end{equation}
$(C > 0)$. V tomto případě je efektivní potenciální energie rovna
\[ U_e = \frac C r + \frac{M^2}{2mr^2}. \]
Tato energie monotonně klesá od $+\infty$ při $r = 0$ až k nule při vzdálenosti jdoucí k $\infty$. Energie částice může být pouze kladná a pohyb je vždy infinitní. Při odvození všech vzorců budeme postupovat zcela stejně jako v minulém případě {\footnotesize [ a taky to vyjde, až na nějaká znamení sem a tam, stejně ] }. Trajektorie částice je hyperbolická:
\begin{equation} \label{15.14}
	r = \frac{p}{e \cos \phi - 1}
\end{equation}
($p$ a $e$ jsou dány vztahy \eqref{15.4}). Jak je ukázáno na obr. 13, dráha částice vůbec střed neobíhá. Perihelium je od něj vzdáleno
\begin{equation} \label{15.15}
	r_\text{min} = a(e+1).
\end{equation}
Závislost polohy na čase je opět dána parametricky rovnicemi
\begin{align} \label{15.16}
	r &= a(e \ch u + 1), &
	t &= \sqrt{\frac{ma^3}{C}} (e \sh u + u),&
	x &= a(\ch u + e), &
	y &= a \sqrt{e^2 - 1} \sh u.
\end{align}
Na závěr tohoto paragrafu ještě ukažme, že pohyb v poli s potenciálem ve tvaru $U = C/r$ (s libovolným $C$) má integrál pohybu specifický pouze pro toto pole. Snadno se prověří přímým výpočtem, že se zachovává
\begin{equation} \label{15.17}
	\vv v \times \vv M + C \vv r / r = \text{const.}
\end{equation}
A skutečně, její úplná časová derivace je rovna
\[ \dot{\vv v} \times \vv M + \frac{C \vv v}{r} - \frac{C \vv r (\vv v \vv r)}{r^3}. \]
Dosadíme $\vv M = m(\vv r \times \vv v)$:
\[ m\vv r (\vv v \dot{\vv v}) - m \vv v (\vv r \dot{\vv v}) + \frac{C \vv v}{r} - \frac{C \vv r (\vv v \vv r)}{r^3}, \]
a jestliže nadto dosadíme podle pohybových rovnic $m \dot{\vv v} = C \vv r / r^3$, zjistíme, že je tento výraz roven nule.

Zachovávající se vektor \eqref{15.17} má směr rovnoběžný s hlavní osou trajektorie, od ohniska k periheliu, a jeho velikost je rovna $Ce$. O tom je možno se přesvědčit, jestliže zjistíme jeho hodnotu v periheliu.

Doplňme ještě, že stejně jako integrály pohybu $\vv M$ a $E$ je i tento nový integrál pohybu jednoznačnou funkcí stavu (souřadnic a rychlosti) částice. V \hyperref[p52]{§ 52} uvidíme, že zjevení takového dodatečného integrálu pohybu souvisí s takzvanou degenerací pohybu.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u15.1} \textbf{1.} Najít závislost souřadnic částice na čase při pohybu v poli $U = -C/r$ s energií $E = 0$ (po parabole):

\textbf{Řešení.} V integrálu
\[ t = \int\frac{r \D r}{\sqrt{\frac{2c}{m} r - \frac{M^2}{m^2}}} \]
uděláme substituci
\[ r = \frac{M^2}{2mC}(1 + \eta^2) = \half p (1 + \eta^2) \]
a nakonec dostaneme žádanou závislost v parametrickém tvaru
\begin{align*}
	r&= \half p (1 + \eta^2),&
	t &= \sqrt{\frac{mp^3}{C}} \frac \eta 2 \left(1 + \frac{\eta^2}{3}\right),\\
	x &= \half p (1 - \eta^2),&
	y &= p\eta.
\end{align*}
Parametr $\eta$ jde od $-\infty$ k $\infty$.

\phantomsection \label{u15.2} \textbf{2.} Integrovat pohybové rovnice hmotného bodu v centrálním poli $U = -C/r^2$, je-li $C>0$.

\textbf{Řešení.} Podle \eqref{14.6} a \eqref{14.7} nalezneme
\begin{ci}
\item při $E > 0, M^2/2m > C$:
	\[ \frac 1 r =\ \sqrt{\frac{2mE}{M^2 - 2mC}} \cos \left[ \phi\ \sqrt{1 - \frac{2mC}{M^2}} \right] \]
\item při $E > 0, M^2/2m < C$:
	\[ \frac 1 r =\ \sqrt{\frac{2mE}{2mC - M^2}} \sh \left[ \phi\ \sqrt{\frac{2mC}{M^2} - 1} \right] \]
\item při $E < 0, M^2/2m <C$:
	\[ \frac 1 r =\ \sqrt{\frac{2m|E|}{2mC - M^2}} \ch \left[ \phi\ \sqrt{\frac{2mC}{M^2} - 1} \right] \]
\end{ci}
Ve všech třech případech platí
\[ t = \frac 1 E\ \sqrt{\frac m 2} \sqrt{Er^2 + C - \frac{M^2}{2m}}. \]
V posledních dvou případech částice padá do středu pole po křivce, která se při $\phi \to \infty$ přibližuje k počátku souřadnic. Ze zadané vzdálenosti $r$ spadne částice do středu za konečný čas, konkrétně
\[ \frac 1 E \ \sqrt{\frac m 2} \left[ \sqrt{C - \frac{M^2}{2m} + Er^2} -\ \sqrt{C - \frac{M^2}{2m}} \right]. \]

\phantomsection\label{u15.3} \textbf{3.} Jestliže k potenciální energii $U = -C/r$ přidáme malou poruchu $\delta U(r)$, přestanou být trajektorie finitních pohybů uzavřené a při každém oběhu se perihelium dráhy otočí o malý úhel $\delta \phi$. Určit $\delta \phi$, je-li: 1. $\delta U = \beta/r^2$, 2. $\delta U = \gamma/r^3$.

\textbf{Řešení.} Při změně $r$ z $r_\text{min}$ na $r_\text{max}$ a opět na $r_\text{min}$ se úhel $\delta \phi$ vyjádří podle vztahu \eqref{14.10}. Přepišme jej do tvaru
\[ \Delta \phi = -2 \pderiv{}{M} \int\limits_{r_\text{min}}^{r_\text{max}} \sqrt{2m(E-U) - \frac{M^2}{r^2}} \D r \]
(abychom se vyhnuli řešení nevlastních integrálů). Položíme nyní $U = -C/r + \delta U$ a rozložíme integrand v mocninnou řadu podle $\delta U$, přičemž druhý a další řád vyhodíme. Absolutní člen dá při integraci $2\pi$ {\footnotesize [ už víme, že částice při $\delta U = 0$ bude obíhat po elipse ] } a člen prvního řádu bude naše žádané stočení:
\[ \delta \phi = \pderiv{}{M} \int\limits_{r_\text{min}}^{r_\text{max}} \frac{2m \delta U \D r}{\sqrt{2m\left(E + \frac C r\right) - \frac{M^2}{r^2}}} = \pderiv{}{M} \left( \frac{2m}{M} \int_0^\pi r^2 \delta U \D \phi \right), \]
přičemž jsme přešli od integrace podle $\D r$ k integraci podle $\D \phi$ podle trajektorie pohybu s $\delta U = 0$.

V případě 1 dostaneme po triviální integraci
\[ \delta \phi = - \frac{2\pi \beta m}{M^2} = -\frac{2\pi\beta}{C p} \]
($p$ je parametr původní elipsy podle \eqref{15.4}). V případě 2 je $r^2 \delta U = \gamma / r$ a po dosazení $r$ podle \eqref{15.5} dostaneme
\[ \delta \phi = -\frac{6\pi C \gamma m^2}{M^4} = - \frac{6\pi \gamma}{Cp^2}. \]
