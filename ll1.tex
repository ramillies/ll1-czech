\documentclass[a4paper,10pt,openany]{book}
\pdfmapfile{=mdugm.map}
\input{sqrtredef}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{fancyhdr}
\usepackage[left=2cm,top=2cm,right=2cm]{geometry}
\usepackage[usenames,dvipsnames]{color}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{xfrac}
\usepackage{tabu}
\usepackage{longtable}
\usepackage{multirow}
\usepackage{siunitx}
\usepackage[urw-garamond]{mathdesign}
\usepackage[T1]{fontenc}
\usepackage{bookmark}
\usepackage{hyperref}
\usepackage{yfonts}
\usepackage{chngcntr}
\usepackage{mathrsfs}
\usepackage{wrapfig}
\usepackage{perpage}

\hypersetup{
	unicode=true,
	pdfstartview={FitH},
	pdftitle={Landau, Lifšic: Mechanika},
	pdfnewwindow=true,
	colorlinks=false,
	linkcolor=red,
	citecolor=green,
	filecolor=magenta,
	urlcolor=cyan,
	pdfborder={0.2 0.2 0.3 [1 1]},
	bookmarksnumbered=true,
	bookmarksopen=true,
	bookmarksopenlevel=1,
	pdfpagemode=UseOutlines,
}
\bookmarksetup{depth=5}
\counterwithout{section}{chapter}
\numberwithin{equation}{section}

\let\arcsin\undefined
\let\arccos\undefined
\let\div\undefined
\let\theta\undefined
\let\phi\undefined
\let\epsilon\undefined
\let\Re\undefined
\let\Im\undefined

\DeclareMathAlphabet{\mathswab}{LY}{yswab}{m}{n}
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}

\DeclareMathOperator{\half}{\sfrac{1}{2}}
\DeclareMathOperator{\qrt}{\sfrac{1}{4}}
\DeclareMathOperator{\Re}{\mathswab{Re}}
\DeclareMathOperator{\Im}{\mathswab{Im}}

\def\D{\mathrm{d}}
\def\I{\mathfrak{i}}
\def\theta{\vartheta}
\def\phi{\varphi}
\def\epsilon{\varepsilon}
\DeclareMathOperator{\tg}{tg}
\DeclareMathOperator{\cotg}{cotg}
\DeclareMathOperator{\arcsin}{arc\ sin}
\DeclareMathOperator{\arccos}{arc\ cos}
\DeclareMathOperator{\arctg}{arc\ tg}
\DeclareMathOperator{\arccotg}{arc\ cotg}
\DeclareMathOperator{\sh}{sh}
\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\tgh}{th}
\DeclareMathOperator{\arsh}{ar\ sh}
\DeclareMathOperator{\arch}{ar\ ch}
\DeclareMathOperator{\arth}{ar\ th}
\DeclareMathOperator{\grad}{\vv{grad}}
\DeclareMathOperator{\rot}{\vv{rot}}
\DeclareMathOperator{\div}{div}

\MakePerPage{footnote}
\newcommand{\fig}[1]{ \begin{center}
	\includegraphics[width=4cm]{fig/#1}

	\emph{Obr. #1}
\end{center}}
\newcommand{\wfig}[2]{ \begin{center}
	\includegraphics[width=#2]{fig/#1}

	\emph{Obr. #1}
\end{center}}
\newcommand{\rightfig}[1]{\begin{wrapfigure}{r}{0.25\textwidth} \fig{#1} \end{wrapfigure}}
\newcommand{\rightwfig}[3]{\begin{wrapfigure}{r}{#3 \textwidth} \wfig{#1}{#2} \end{wrapfigure}}
\newcommand{\vv}[1]{\mathswab{#1}}
\newcommand{\vm}[1]{\mathpzc{#1}}
\newcommand{\deriv}[2]{\frac{\D #1}{\D #2}}
\newcommand{\nderiv}[3]{\frac{\D^{#1} #2}{\D #3^{#1}}}
\newcommand{\pderiv}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\npderiv}[3]{\frac{\partial^{#1} #2}{\partial #3^{#1}}}
\newcommand{\example}[2]{\phantomsection \label{u#1.#2} \textbf{#2.}}
\def\hpi{\sfrac{\pi}{2}}
\def\lagr{\mathscr L}
\def\ham{\mathscr H}

\renewcommand{\thepart}{\Alph{part}}
\renewcommand{\thechapter}{\Roman{chapter}}
\addto\captionsczech{\renewcommand{\chaptername}{}}
\renewcommand{\numberline}[1]{\makebox[1.5em][r]{#1\hspace*{0.5em}}}% The Roman

\makeatletter
\let\Hy@linktoc\Hy@linktoc@none
\newcommand*{\bettersymbol}[1]{%
  \ensuremath{%
    \ifcase#1% 0
    \or % 1
      \star%   
    \or % 2
      \dagger
    \or % 3  
      \ddagger
    \or % 4   
      \mathsection
    \or % 5
      \mathparagraph
    \else % >= 6
      \@ctrerr  
    \fi
  }%   
}   
\makeatother


\begin{document}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{\nouppercase{\rightmark}}
\fancyfoot[L]{}
\fancyfoot[C]{\thepage}
\selectlanguage{czech}
\setlength{\parindent}{0.5cm}
\setlength{\parskip}{0.05cm}

\newenvironment{cd}{
\begin{description}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{description}}
\newenvironment{ce}{
\begin{enumerate}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{enumerate}}
\newenvironment{ci}{
\begin{itemize}
\setlength{\itemsep}{0.1cm}
\setlength{\parskip}{0cm}
\setlength{\parsep}{0cm}}{\end{itemize}}
\newcommand{\cara}{\rule{\linewidth}{0.4mm}}

\renewcommand*{\thefootnote}{\bettersymbol{\value{footnote}}}

\sisetup{
	exponent-product=\cdot,
	output-decimal-marker={,},
	angle-symbol-over-decimal,
	group-digits=true,
	group-separator=\text{~},
	group-minimum-digits=4,
	detect-all,
}

\setcounter{secnumdepth}{3}

\tableofcontents

\chapter{Pohybové rovnice}
\input{kap1}

\chapter{Zákony zachování}
\input{kap2}

\chapter{Integrování pohybových rovnic}
\input{kap3}

\chapter{Srážky částic}
\input{kap4}

\chapter{Harmonické kmity}
\input{kap5}

\end{document}
