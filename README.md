# Landau, Lifšic: Mechanika

Jde o český překlad slavné ruské učebnice. Byl jsem toho názoru, že by tento text měl být přeložen do češtiny (a na některých místech trochu doplněn komentáři (zejména po využití frází „Je zřejmé, že“ atd.)), a tak jsem to začal dělat.

Pokud si chcete prostě přečíst text v nejúplnějším dostupném stavu, berte soubor „ll1.pdf“ (uvidíte ho v nabídce výše). 

Jestliže si ho chcete sami zkompilovat, tak si repositář naklonujte. Mělo by to jít přeložit tak, jak to leží a běží, alespoň pokud máte úplnou instalaci TeXLive (na systémech à la Debian zkuste nainstalovat balík jménem `texlive-full`). Ke kompilaci využijte `pdflatex`.

Je-li vaším přáním opravit přehmaty nebo překlepy, jsou dvě možnosti. Buď si můžete repositář naforkovat a zadat pull-request, nebo napsat e-mail na školní adresu 433714 zavinac mail tecka muni tecka cz (vyjadřujte se přitom prosím co možná nejstručněji a nejjasněji).

Na závěr **prosba**: Jestliže pro vás bude kterékoli místo těžko stravitelné nebo vám přijde příliš odfláknuté, popište mi to prosím podrobně (ale stručně) e-mailem (na výše uvedenou adresu); já pak přidám vysvětlující poznámku překladatele. Nezdráhejte se, poznámky překladatele jsou vlastně ten hlavní důvod, proč celou práci dělám :—). (Na druhou stranu: pokud vám přijde *každé* místo obtížně stravitelné a těžkopádné a zasekáváte se na každé druhé formulce, pak lituji, ale tohle není knížka pro vás. Na L&L je super, že se s látkou moc nemaže a neokecává každou trivialitu, takže se dostane i k zajímavějším věcem. Knih, které každou trivialitu okecávají, je veliká hora, takže nepochybuji, že, pakliže vám stručnost výkladu vadí, najdete některou, která bude vyhovovat lépe vaším zájmům. (Následuje velmi významný pohled na HRW.))

## Statistika postupu

**Paragrafy**: 25 / 52

**Přeložené strany**: 95 / 205 (46 %)

**Počet stran výstupu**: 61

## Citát

*Как  однажды пошутил автор по поводу заданного ему вопроса: "Как Вы решились  написать учебник по теорфизике, когда Ландау о всем написал?" "Я пишу о  том, о чем Ландау сказал, что это легко показать" - ответил он.*

*Jednou se ho* (autora jiného kursu teoretické fysiky) *tázali: „Jak Vás napadlo psát učebnici teoretické fysiky, když už Landau o všem napsal?“ Na to on pravil: „Já píšu o tom, o čem Landau řekl, že je to evidentní.“*
