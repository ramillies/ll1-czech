\section{Energie} \label{p6}

Při pohybu mechanické soustavy se obecně vzato všech $2s$ veličin $q_i$ a $\dot q_i$, které určují její stav, mění s průběhem času. Existují ovšem takové funkce těchto veličin, které mají během celého pohybu konstantní hodnotu, která závisí jen na počátečních podmínkách. Takovým funkcím se říká \emph{integrály pohybu}.

Počet nezávislých integrálů pohybu je pro uzavřenou mechanickou soustavu se $s$ stupni volnosti roven $2s - 2$. To je zřejmé z následujících prostých úvah. Obecné řešení pohybvých rovnic obsahuje $2s$ integračních konstant. Pokud pohybové rovnice explicitně nezávisí na čase, je vždy výběr toho, v kterém okamžiku bude čas $t = 0$, libovolný, a jedna z konstant při jejich řešení může být vybrána právě jako konstanta, která se přičítá k času. Jestliže v $2s$ funkcích tvaru
\begin{gather*}
	q_i = q_i(t + t_0, C_1, C_2, \ldots, C_{2s-1}),\\
	\dot q_i = \dot q_i(t + t_0, C_1, C_2, \ldots, C_{2s-1})
\end{gather*}
vyloučíme $t+t_0$, zůstane nám $2s-1$ libovolných konstant, které pak vyjádříme ve tvaru funkcí z $q$ a $\dot q$; a to budou naše integrály pohybu.

Je ale zřejmé, že některé integrály pohybu mají v mechanice důležitější roli než jiné, a je mezi nimi několik, které mají velmi hlubokou vazbu na základní vlastnosti prostoru i času --- jejich homogenitu a isotropnost. Kromě toho mají všechny tyto veličiny velmi užitečnou vlastnost aditivity: pro soustavu skládající se z několika částí, jejichž vzájemné interakce je možno zanedbat, se jejich hodnoty rovnají součtům hodnot pro každou z těchto částí zvlášť.

Řečená vlastnost aditivity těmto veličinám přisuzuje zvláštní roli. Například předpokládejme, že spolu dvě tělesa v nějakém okamžiku interagují. Jelikož ovšem před interakcí i po ní platilo, že ta či ona veličina je součtem příslušných veličin pro obě tělesa zvlášť, dávají nám zákony zachování možnost udělat řadu závěrů o chování těles po interakci, pokud víme, v jakém stavu byla před ní.

Začněme se zákonem zachování, který je vázán na \emph{homogenitu času}.

Díky této homogenitě lagrangián nezávisí explicitně na čase. Úplná časová derivace lagrangiánu pak může být zapsána tímto způsobem:
\[ \deriv \lagr t = \sum_i \pderiv{\lagr}{q_i} \dot q_i + \sum_i \pderiv{\lagr}{\dot q_i} \ddot q_i \]
(kdyby lagrangián na čase explicitně závisel, ještě by se na pravou stranu muselo přičíst $\partial \lagr / \partial t$). Podle Lagrangeových rovnic zaměníme $\partial \lagr / \partial q_i$ za $\deriv{}{t} \pderiv{\lagr}{\dot q_i}$ a obdržíme
\[ \deriv \lagr t = \sum_i \dot q_i \deriv{}{t} \pderiv{\lagr}{\dot q_i} + \sum_i \pderiv{\lagr}{\dot q_i} \ddot q_i = \sum_i \deriv{}{t}\left(\pderiv{\lagr}{\dot q_i} \dot q_i\right), \]
nebo-li
\[ \deriv{}{t} \left(\sum_i \dot q_i \pderiv{\lagr}{\dot q_i} - \lagr \right) = 0. \]
Z toho už vidíme, že veličina
\begin{equation} \label{6.1}
	E = \sum_i \dot q_i \pderiv{\lagr}{\dot q_i} - \lagr
\end{equation}
po celou dobu pohybu uzavřené soustavy zůstává neměnnou a že je tudíž jedním z integrálů pohybu. Tato veličina se nazývá \emph{(zobecněnou) energií} soustavy. Její aditivita přímo vyplývá z aditivity lagrangiánu, na němž závisí podle \eqref{6.1} jen lineárně.

Zákon zachování energie platí nejen pro uzavřené soustavy, ale i pro soustavy, které se nacházejí ve stacionárním (tedy časově nezávislém) vnějším poli: jediný náš předpoklad, explicitní nezávislost lagrangiánu na čase, je splněn i v tomto případě. Mechanické soustavy, jejichž energie se zachovává, se někdy nazývají \emph{konservativními}. {\footnotesize [ To je celkem nešťastný název. Čtenář nechť si to neplete s konservativními \emph{poli}, neboť je to něco úplně jiného. ] }

Jak už jsme viděli v \hyperref[p5]{§ 5}, lagrangián uzavřené soustavy (či takové, která se nalézá ve stacionárním poli) má tvar
\[ \lagr = T(q, \dot q) - U(q), \]
kde $T$ je kvadratická funkce rychlostí. Podle známé Eulerovy věty o homogenních funkcích dostaneme
\[ \sum_i \dot q_i \pderiv{\lagr}{\dot q_i} = \sum_i \dot q_i \pderiv{T}{\dot q_i} = 2T. \]
Jestliže to dosadíme do \eqref{6.1}, dostaneme
\begin{equation} \label{6.2}
E = T(q, \dot q) + U(q);
\end{equation}
v kartézských souřadnicích tedy
\begin{equation} \label{6.3}
	E = \sum_a \half m_a v_a^2 + U(\vv r_1, \vv r_2, \ldots).
\end{equation}

{\footnotesize [ Pro zopakování: \emph{homogenní} funkce stupně $k$ je taková funkce několika proměnných, jejíž hodnota se, zvětší-li se všechny její argumenty $t$-krát, zvětší $t^k$-krát. Eulerova věta o homogenních funkcích pak praví, že pro homogenní funkci $f$ $k$-tého stupně platí rovnost

\[ \sum_i x_i \pderiv{f(x_1, x_2, \ldots)}{x_i} = kf(x_1, x_2, \ldots). \]

Důkaz se dostane po derivaci definičního vztahu pro homogenní funkci ($f(tx_1, tx_2, \ldots) = t^k f(x_1, x_2, \ldots)$) podle $t$ a položením $t = 1$. ]}

Energie soustavy tedy může být zapsána jako součet dvou různých částí: kinetické energie závislé na rychlostech a potenciální energie závislé pouze na polohách.

\section{Hybnost} \label{p7}

Jiný zákon zachování vzniká jako důsledek \emph{homogenity prostoru}.

Díky této homogenitě se mechanické vlastnosti uzavřené soustavy nemění při rovnoběžném posunutí v prostoru celé soustavy jako celku. Rozebereme tedy nekonečně malý posun $\vv e$ a budeme požadovat, aby lagrangián zůstal nezměněn. {\footnotesize [ \textbf{POZOR! VAROVÁNÍ!} Na tomto místě se hodí zmínit, že dále budeme postupovat v kartézských souřadnicích. Autoři na to nějak zapomněli. ] }

Rovnoběžný přenos se dělá tak, že se všechny body soustavy přemístí o týž vektor, v našem případě $\vv e$; jejich polohové vektory se tedy z $\vv r_a$ změní na $\vv r_a + \vv e$. Změna lagrangiánu po takovém posunu je rovna
\[ \delta \lagr = \sum_a \pderiv{\lagr}{\vv r_a} \delta \vv r_a = \vv e \sum_a \pderiv{\lagr}{\vv r_a}, \]
kde se suma provádí přes všechny hmotné body soustavy. Jelikož vektor $\vv e$ může být libovolný, je požadavek $\delta \lagr = 0$ ekvivalentní požadavku
\begin{equation} \label{7.1}
	\sum_a \pderiv{\lagr}{\vv r_a} = 0.
\end{equation}

Podle Lagrangeových rovnic ve tvaru \eqref{5.2} můžeme tento výraz přepsat na
\[ \sum_a \deriv{}{t} \pderiv{\lagr}{\vv v_a} = \deriv{}{t} \sum_a \pderiv{\lagr}{\vv v_a} = 0. \]

Tímto způsobem tedy zjistíme, že se v uzavřené mechanické soustavě zachovává veličina
\begin{equation} \label{7.2}
	\vv P = \sum_a \pderiv{\lagr}{\vv v_a},
\end{equation}
které se říká \emph{hybnost}. \footnote{[ Čas od času se lze setkat i s označením \clqq impuls\crqq\ (zejména v cizích jazycích). ]} Budeme-li diferencovat lagrangián \eqref{5.1}, naleznme, že se hybnost vyjádří pomocí rychlostí jednotlivých bodů soustavy takto:
\begin{equation} \label{7.3}
	\vv P = \sum_a m_a \vv v_a.
\end{equation}

Aditivnost hybnosti by měla být zřejmá. Kromě toho pro ni, jak vidíme, narozdíl od energie platí, že celková hybnost soustavy je rovna součtu hybností
\[ \vv p_a = m_a \vv v_a \]
všech jednotlivých částic, aniž by při tom hrálo roli to, jestli částice mezi sebou interagují či nikoli.

Zákon zachování všech tří souřadnic vektoru hybnosti platí pouze v případě, že na částice už nepůsobí žádné vnější pole. Jestliže takové vnější pole na soustavu působí, mohou se zachovávat pouze ty složky hybnosti, na jejichž směrech potenciální energie nezávisí. Třeba v tíhovém poli, které má směr opačný k ose $z$, se bude zachovávat $x$-ová a $y$-ová komponenta hybnosti. {\footnotesize [ Je to jasné i z našeho předpokladu homogenity prostoru. Jestliže posuneme soustavu kolmo na působení tíhového pole, nestane se žádná změna, ale při posunu ve směru síly už e bude měnit potenciální energie soustavy a to našim požadavkům odporuje. ] }

Výchozí rovnice \eqref{7.1} má velmi jednoduchý fysikální smysl. Derivace $\partial \lagr / \partial \vv r_a$ je síla $\vv F_a$ působící na $a$-tou částici. Rovnice \eqref{7.1} tedy říká, že součet sil, které působí na všechny částice uzavřené soustavy, má být rovna nule:
\begin{equation} \label{7.4}
	\sum_a \vv F_a = 0.
\end{equation}
Speciálně pro soustavu sestávající ze dvou částic platí $\vv F_1 + \vv F_2 = 0$: síla, kterou působí jedna částice na druhou, musí být zřejmě rovna velikostí, avšak opačná směrem, síle, kterou působí druhá částice na první. Toto tvrzení je známo jako \emph{třetí Newtonův zákon} nebo též \emph{zákon akce a reakce}.

Jestliže pohyb popisujeme místo kartézských nějakými jinými zobecněnými souřadnicemi $q_i$, nazveme derivace lagrangiánu podle zobecněných rychlostí
\begin{equation} \label{7.5}
	p_i = \pderiv{\lagr}{\dot q_i}
\end{equation}
\emph{zobecněnými hybnostmi} a derivace
\begin{equation} \label{7.6}
	F_i = \pderiv{\lagr}{q_i}
\end{equation}
\emph{zobecněnými silami}. Při takovém značení pak mají Lagrangeovy rovnice tvar
\begin{equation} \label{7.7}
	\dot p_i = F_i.
\end{equation}

{\footnotesize [ Zde se zcela přechází mlčením (bůhvíproč?) dost zásadní fakt. Jestliže vidíme, že lagrangián vůbec nezávisí na některé souřadnici $q_i$, ale jen na příslušné rychlosti $\dot q_i$, říkáme příslušné souřadnici \emph{cyklická} a hned víme, že se jí náležející hybnost $p_i$ zachovává. Důkaz je jednoduchý: když lagrangián na té souřadnici nezávisí, pak musí být nutně jeho derivace podle ní nulová. To odpovídá podle \eqref{7.6} $F_i = 0$ a z Lagrangeových rovnic ve tvaru \eqref{7.7} hned vidíme, že $\dot p_i = 0$.

Podle mě je právě toto tvrzení tím, oč by mělo v této kapitole jít především. Jedním z důvodů, proč tyhle poznámky píšu, je ten, že občas v knížce něco nesmyslně chybí a hodí se, když to někdo doplní. Jako třeba tady. ] }

V kartézských souřadnicích jsou zobecněné hybnosti rovny jednotlivým složkám vektorů $\vv p_a$. V obecném případě jsou hybnosti $p_i$ lineárními homogenními funkcemi zobecněných rychlostí $\dot q_i$ a neplatí nutně, že by byly rovny prostým součinům hmotnosti a rychlosti.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u7.1} Částice s hmotnosti $m$, která se pohybuje rychlostí $\vv v$, přechází z poloprostoru, v němž je potenciální energie konstantní a rovna $U_1$, do poloprostoru, v němž je tato energie rovněž konstantní, ale rovna $U_2$. Určit, jak se změní směr postupu částice.

\textbf{Řešení.} Potenciální energie nezávisí vůbec na souřadnicích, které jsou rovnoběžné s rovinou oddělující oba řečené poloprostory. Z tohoto důvodu se tedy musí zachovávat projekce hybnosti částice na tuto rovinu. Označíme symboly $\phi_1$ a $\phi_2$ úhly mezi normálou k této rovině a rychlostmi $\vv v_1$ a $\vv v_2$, které má částice před a po přechodu. Jelikož se hybnost kolmo na rozdělující rovinu zachovává, dostaneme $v_1 \sin \phi_1 = v_2 \sin \phi_2$. Rychlosti $v_1$ a $v_2$ jsou mezi sebou vázány zákonem zachování energie a nakonec obdržíme
\[ \frac{\sin \phi_1}{\sin\phi_2} = \sqrt{1 + \frac{2}{mv_1^2} \left(U_1 - U_2\right) }. \]

\normalsize

\section{Těžiště} \label{p8}

{\footnotesize [ \textbf{POZOR! VAROVÁNÍ!} Kartézské souřadnice. ] }

Hybnost uzavřené soustavy má různé hodnoty pro různé inerciální vztažné soustavy. Jestliže se totiž soustava $K'$ pohybuje vzhledem k jiné soustavě $K$ s rychlostí $\vv V$, pak jsou rychlosti $\vv v'_a$ a $\vv v_a$ navzájem vázány vztahem $\vv v_a = \vv v'_a + \vv V$. Vztah mezi hybnostmi $\vv P$ a $\vv P'$ je tedy takový:
\[ \vv P = \sum_a m_a \vv v_a = \sum_a m_a \vv v'_a + \vv V \sum_a m_a, \]
čili
\begin{equation} \label{8.1}
	\vv P = \vv P' + \vv V \sum_a m_a.
\end{equation}

Vždy tedy musí existovat taková soustava $K'$, v níž je hybnost nulová. Položíme v \eqref{8.1} $\vv P' = 0$ a dostaneme pro rychlost této soustavy vzhledem k \clqq naší\crqq\ soustavě $K$:
\begin{equation} \label{8.2}
	\vv V = \frac{\vv P}{\sum m_a} = \frac{\sum m_a \vv v_a}{\sum m_a}.
\end{equation}

Jestliže je celková hybnost mechanické soustavy rovna nule, pak se říká, že je tato soustava vzhledem k té či oné vztažné soustavě v klidu. To je ostatně přirozené zobecnění pojmu klidu, který už máme pro jeden bod, a rychlost $\vv V$, která je dána vztahem \eqref{8.2}, stejně tak zobecňuje pohyb mechanické soustavy \clqq jako celku\crqq. Vidíme tedy, že zákon zachování hybnosti nám umožní takto pěkně rozdělit pohyb soustavy na dva: jednak jejího pohybu jako celku, jednak pohyby \clqq uvnitř\crqq\ soustavy.

Vzorec \eqref{8.2} ukazuje, že vztah mezi hybností $\vv P$ a rychlostí $\vv V$ soustavy jako celku je stejný, jako by byl mezi hybností a rychlostí jedné hmotné částice s hmotností $M = \sum m_a$, která je rovna součtu hmotností všech částic v soustavě. Jde vlastně o tvrzení o \emph{aditivnosti hmotností}.

Vztah \eqref{8.2} můžeme také přepsat jako časovou derivaci výrazu
\begin{equation} \label{8.3}
	\vv R = \frac{\sum m_a \vv r_a}{\sum m_a}.
\end{equation}
Je tedy možno říci, že rychlost soustavy jako celku je rychlost pohybu bodu, jehož polohový vektor je dán právě vzorcem \eqref{8.3}. Tento bod se nazývá \emph{těžištěm} (někdy též \emph{hmotným středem}) soustavy.

Zákon zachování hybnosti uzavřené soustavy je tedy možno formulovat i tak, že se těžiště této soustavy pohybuje rovnoměrně přímočaře. Jde tedy o zobecnění zákona setrvačnosti, který jsme odvodili v \hyperref[p3]{§ 3} pro jednu volnou částici, na ten způsob, že místo této \clqq volné částice\crqq\ uvažujeme právě těžiště soustavy.

Při řešení mechanických úloh o uzavřených soustavách je celkem přirozené pracovat právě v té vztažné soustavě, v níž je těžiště mechanické soustavy v klidu. Tím totiž odstraníme ze svých úvah pohyb, který je všem bodům soustavy společný.

Energie soustavy, která je jako celek v klidu, se často nazývá její \emph{vnitřní energií} $E_v$. Ta obsahuje kinetickou energii relativního pohybu částic v soustavě a potenciální energii jejich vzájemných interakcí. Úplná energie soustavy, která se pohybuje jako celek rychlostí $V$, může být napsána ve tvaru
\begin{equation} \label{8.4}
	E = \half \mu V^2 + E_v.
\end{equation}
Poskytněme ještě přímé odvození tohoto vztahu. Energie $E$ a $E'$ mechanické soustavy ve dvou různých inerciálních soustavách $K$ a $K'$ jsou vázány rovnicí
\[ E = \half \sum_a m_a \vv v_a^2 + U = \half \sum_a m_a (\vv v_a' + \vv V)^2 + U = \half \mu V^2 + \vv V \sum_a m_a \vv v_a' + \half sum_a m_a v_a'^2 + U, \]
nebo-li
\begin{equation} \label{8.5}
	E = E' + \vv V + \vv P' + \half \mu V^2.
\end{equation}
To je zákon transformace energií při přechodu od jedné inerciální soustavy k druhé, podobně, jako jsme měli pro hybnost zcela podobnou formulku \eqref{8.1}. Je-li v soustavě $K'$ těžiště v klidu, pak je $\vv P' = 0$ a $E' = E_v$, což, dosazeno výše, dá opět \eqref{8.4}.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u8.1} Najít zákon transformace akce při přechodu od jedné inerciální soustavy k druhé.

\textbf{Řešení.} Lagragián je roven rozdílu kinetické a potenciální energie, takže postupem zcela analogickým, kterým jsme dostali \eqref{8.5}, obdržíme i
\[ \lagr = \lagr' + \vv V \vv P' + \half \mu V^2. \]
Jelikož $S = \int \lagr \D t$, stačí integrovat obě strany a máme výsledek
\[ S = S' + \mu \vv V \vv R' + \half \mu V^2 t, \]
kde $\vv R'$ je polohový vektor těžiště v soustavě $K'$.

\normalsize

\section{Moment hybnosti} \label{p9}

{\footnotesize [ \textbf{POZOR! VAROVÁNÍ!} Do třetice všeho dobrého i zlého --- kartézské souřadnice. Navíc nově pouze ve třech rozměrech! ] }

Nyní se vydejme odvodit zákon zachování, který je důsledkem \emph{isotropie prostoru}, tedy jeho stejných vlastnostech ve všech směrech.

Tento požadavek může být též formulován tak, že se mechanické vlastnosti uzavřené soustavy nemají měnit při libovolném prostorovém otočení soustavy jako celku. V souladu s tím vyšetříme, co udělá nekonečně malé otočení soustavy, a opět budeme vyžadovat, aby se tím lagrangián nezměnil.

Zavedeme vektor otočení $\vv o$, jehož velikost bude rovna úhlu $\delta\phi$ otočení a jehož směr bude rovnoběžný s osou otáčení. (Konkrétně si vybereme tak, aby platilo pravidlo pravé ruky, tj. vektor bude směřovat v tu stranu, odkud se otočení jeví jako jdoucí proti směru hodin.)

\begin{wrapfigure}{r}{0.2\textwidth}
	\begin{center}
	\includegraphics[width=2cm]{fig/5}

	\vspace{-2mm}
	\emph{Obr. 5}
	\end{center}
\end{wrapfigure}

Ze všeho nejdřív najdeme, čemu je při takovém otočení roven přírůstek polohového vektoru spojujícího počátek souřadnic (který pro jednoduchost předpokládejme v ose otáčení) a libovolný bod otáčející se soustavy. Pro velikost tohoto přírůstku platí (jak vidno z obrázku 5)
\[ | \delta \vv r | = r \sin \theta \delta \phi. \]
Směr tohoto vektoru á být přitom kolmý jak k $\vv r$, tak k ose $\vv o$. Teď už je zřejmé, že musí být
\begin{equation} \label{9.1}
	\delta \vv r = \vv o \times \vv r.
\end{equation}
Při otočení se nemění jen polohy, ale úplně stejným způsobem se mění i rychlosti. Změna rychlosti vzhledem k nepohyblivé soustavě souřadnic tedy je {\footnotesize [ dostane se to jednoduchým derivováním \eqref{9.1} podle času; $\vv o$ je konstanta ] }
\begin{equation} \label{9.2}
	\delta \vv v = \vv o \times \vv v.
\end{equation}

Tyto výrazy nyní dosadíme do požadavku na to, aby se lagrangián při takovém otočení nezměnil:
\[ \delta \lagr = \sum_a \left[ \pderiv{\lagr}{\vv r_a} \delta \vv r_a + \pderiv{\lagr}{\vv v_a} \delta \vv v_a \right] = 0, \]
zaměníme $\partial \lagr / \partial \vv v_a = \vv p_a$ a $\partial \lagr / \partial \vv r_a = \dot{\vv p_a}$:
\[ \sum_a [\dot{\vv p_a} \cdot (\vv o \times \vv r_a) + \vv p_a \cdot (\vv o \times \vv v_a)] = 0, \]
v prvním smíšeném součinu uděláme cyklickou záměnu {\footnotesize [ při té se hodnota smíšeného součinu nemění. Nejsnáz to uvidíte, když si smíšený součin napíšete jako determinant. ] } a vytkneme konstantní $\vv o$ před znamení sumy:
\[ \vv o \sum_a ( \vv r_a \times \dot{\vv p_a} + \vv v_a \times \vv p_a ) = \vv o \deriv{}{t} \sum_a \vv r_a \times \vv p_a = 0. \]
Jelikož $\vv o$ byl vybrán libovolně, musí být
\[ \deriv{}{t} \sum_a \vv r_a \times \vv p_a = 0, \]
a z toho hned dostáváme, že se při pohybu uzavřené soustavy má zachovávat vektorová veličina
\begin{equation} \label{9.3}
	\vv M = \sum_a \vv r_a \times \vv p_a,
\end{equation}
která se nazývá \emph{moment hybnosti} soustavy. Jeho aditivnost je opět zřejmé z toho, že má tvar sumy, a kromě toho, stejně jako hybnost, nezávisí na přítomnosti či nepřítomnosti interakcí mezi částicemi.

Tím jsme vyčerpali aditivní integrály pohybu. Každá uzavřená soustava jich tedy má celkem sedm: energii a tři složky vektorů hybnosti a momentu hybnosti.

Jelikož ovšem ve vztahu pro moment hybnosti vystupují polohové vektory, musí jeho hodnota obecně záviset na tom, jak zvolíme počátek souřadnic. Pokud máme dvě soustavy souřadnic, jejichž počátky jsou od sebe o vektor $\vv a$, pak jsou polohové vektory jednoho a téhož bodu v těchto soustavách vázány vztahem (třeba) $\vv r_a = \vv r'_a + \vv a$. Pak dostaneme pro moment:
\[ \vv M = \sum_a \vv r_a \times \vv p_a = \sum_a \vv r'_a \times \vv p_a + \vv a \times \left(\sum_a \vv p_a\right) \]
nebo-li
\begin{equation} \label{9.4}
	\vv M = \vv M' + \vv a \times \vv P.
\end{equation}
Z této rovnice je vidět, že moment hybnosti nezávisí na výběru počátku souřadnic pouze v případě, že pohyb vyšetřujeme ve vztažné soustavě, ve které je mechanická soustava jako celek v klidu. Na zákonu zachování se ovšem tento fakt nijak neprojeví, neboť i hybnost se pro uzavřené soustavy zachovává.

Ještě odvodíme vztah, který váže momenty hybnosti ve dvou různých inerciálních soustavách $K$ a $K'$, kde $K'$ sepohybuje vzhledem ke $K$ rychlostí $\vv V$. Předpokládejme, že v daný okamžik jsou počátky souřadnic v obou soustavách v témže bodě prostoru. Polohové vektory částic v obou soustavách jsou tedy stejné, ale jejich rychlosti váže vztah $\vv v_a = \vv v'_a + \vv V$. Máme tedy
\[ \vv M = \sum_a m_a \vv r_a \times \vv v_a = \sum_a m_a \vv r_a \times \vv v'_a + \sum_a m_a \vv r_a \times \vv V. \]
První součet na pravé straně rovnice je moment hybnosti $\vv M'$ v soustavě $K'$. Nahradíme-li navíc součet polohových vektorů polohou těžiště podle \eqref{8.3}, dostaneme
\begin{equation} \label{9.5}
	\vv M = \vv M' + \mu \vv R \times \vv V.
\end{equation}
Tento vzorec je zákonem transformace momentu hybnosti při přechodu mezi dvěma inerciálními soustavami. Je ostatně analogický těm, které jsme odvodili pro podobnou transformaci energie a hybnosti v \eqref{8.1} a \eqref{8.5}.

Je-li inerciální soustava $K'$ právě tou, v níž je daná mechanická soustava jako celek v klidu, pak je $\vv V$ rychlost těžiště soustavy v $K$ a $\mu \vv V$ tedy hybnost $\vv P$ (vzhledem k soustavě $K$).

Pak tedy platí
\begin{equation} \label{9.6}
	\vv M = \vv M' + \vv R \times \vv P.
\end{equation}
Jinými slovy, moment hybnosti $\vv M$ mechanické soustavy se skládá z jakéhosi jejího \clqq vlastního momentu\crqq\ (vzhledem k inerciální soustavě, v níž je v klidu) a z momentu $\vv R \times \vv P$, který je dán pohybem soustavy jako celku.

I když zákon zachování všech tří složek vektoru momentu hybnosti (vzhledem k libovolnému počátku souřadnic) platí jen v uzavřených soustavách, může mít v jistém omezenějším tvaru mít platnost i při působení vnějšího pole. Z našich odvození výše je zřejmé, že se vždy budou zachovávat projekce momentu na takové osy, vůči nimž je dané pole symetrické (pak se totiž při libovolném otáčení kolem této osy působení pole nezmění). 

Nejdůležitějším případem takového druhu je jistě středově symetrické pole, tedy pole, v němž potenciální energie závisí pouze na vzdálenosti k jednomu bodu v prostoru (středu pole). Při pohybu v takovém poli se zřejmě zachovávají projekce momentu na libovolnou osu, která prochází středem. Jinými slovy, zachovává se vektor $\vv M$ momentu hybnosti, ale nikoli vzhledem k libovolně danému počátku souřadnic, nýbrž vzhledem ke středu pole.

Jiný příklad: homogenní pole ve směru osy $z$, ve kterém se zachovává projekce momentu na osu $z$, přičemž se počátek souřadnic může vybrat libovolně.

Ještě poznamenejme, že projekce momentu na libovolnou osu, nazvěme ji $z$, může být nalezena diferencováním lagrangiánu podle vzorce
\begin{equation} \label{9.7}
	M_z = \sum_a \pderiv{\lagr}{\dot\phi_a},
\end{equation}
kde $\phi$ je úhel otočení kolem osy $z$. To je jasné už z charakteru výše uvedeného odvození zákona zachování momentu hybnosti, ale je možno se o tom přesvědčit i přímým výpočtem. Ve válcových souřadnicích máme (po dosazení $x_a = r_a \cos \phi_a, y_a = r_a \sin \phi_a$):
\begin{equation} \label{9.8}
	M_z = \sum_a m_a(x_a \dot y_a - y_a \dot x_a) = \sum_a m_a r^2_a \dot \phi_a.
\end{equation}
Z druhé strany má ovšem lagrangián tvar
\[ \lagr = \half \sum_a m_a (\dot r_a^2 + r_a^2 \dot \phi_a^2 + \dot z_a^2) - U \]
a to, když dosadíme do \eqref{9.7}, vydá opět výraz \eqref{9.8}.

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u9.1} \textbf{1.} Najít výrazy pro kartézské složky a velikost vektoru momentu hybnosti částice ve válcových souřadnicích $r, \phi, z$.

\textbf{Odpověď:}
\begin{align*}
	M_x &= m \sin \phi (r \dot z - z \dot r) - mrz \dot \phi \cos \phi, \\
	M_y &= m \cos \phi (z \dot r - r \dot z) - mrz \dot \phi \sin \phi, \\
	M_z &= mr^2 \dot \phi,\\
	M^2 &= m^2 r^2 \dot \phi^2 (r^2 + z^2) + m^3 (r \dot z - z \dot r)^2.
\end{align*}

\phantomsection \label{u9.2} \textbf{2.} Totéž ve sférických souřadnicích $r, \phi, \psi$.

\textbf{Odpověď:}
\begin{align*}
	M_x &= - mr^2 ( \dot\phi \sin \psi + \dot \psi \sin \phi \cos \phi \cos \psi), \\
	M_y &= mr^2 (\dot \phi \cos \psi - \dot \psi \sin \phi \cos \phi \sin \psi), \\
	M_z &= mr^2 \sin^2 \phi \dot \psi,\\
	M^2 &= m^2 r^4 (\dot \phi^2 + \sin^2 \phi \dot \psi ^2).
\end{align*}

\phantomsection \label{u9.3} \textbf{3.} Které složky vektoru hybnosti $\vv P$ a momentu hybnosti $\vv M$ se zachovávají při pohybu v následujících polích?
\begin{ce}
\item Pole nekonečné homogenní roviny.
\item Pole nekonečného homogenního válce.
\item Pole nekonečného homogenního hranolu.
\item Pole dvou bodů.
\item Pole nekonečné homogenní poloroviny.
\item Pole homogenního kužele.
\item Pole homogenního toru.
\item Pole nekonečné homogenní válcové šroubovice.
\end{ce}

\textbf{Odpovědi:}
\begin{ce}
\item hybnost: $x, y$, moment: $z$ (je-li nekonečná rovina rovnoběžná s rovinou $xy$).
\item hybnost: $z$, moment: $z$ (osou válce je osa $z$).
\item hybnost: $z$ (plášť hranolu rovnoběžný s osou $z$).
\item moment: $z$ (osu $z$ stanovíme tak, aby procházela oběma body).
\item hybnost: $y$ (polorovina je část roviny $xy$ ohraničená osou $y$).
\item moment: $z$ (osou kužele je osa $z$).
\item moment: $z$ (osou toru je osa $z$).
\item \textbf{Řešení.} Lagrangián se nemění při otočení o $\delta \phi$ kolem osy šroubovice (řekněme osy $z$) a současném posunu ve směru této osy o $h \delta \phi / 2\pi$, kde $h$ je stoupání šroubovice. Pak
	\[ \delta \lagr = \pderiv \lagr z \delta z + \pderiv \lagr \phi \delta \phi = \left(\dot P_z \frac{h}{2\pi} + \dot M_z \right) \delta \phi = 0, \]
	z čehož máme
	\[ M_z+ \frac{h}{2\pi} P_z = \text{const.} \]
\end{ce}

\normalsize

\section{Mechanická podobnost} \label{p10}

Vynásobení lagrangiánu libovolnou konstantou, jak už víme z \hyperref[p2]{§ 2}, nemění výsledné pohybové rovnice. I tento zdánlivě triviální fakt nám dává možnost v řadě případů dojít k důležitým závěrům, aniž bychom museli vůbec integrovat pohybové rovnice.

Uvažujme případy, v nichž je potenciální energie homogenní funkcí souřadnic, tedy takové, v nichž platí
\begin{equation} \label{10.1}
	U(t \vv r_1, t \vv r_2, \ldots, t \vv r_n) = t^k U(\vv r_1, \vv r_2, \ldots, \vv r_n),
\end{equation}
přičemž $t$ je libovolná konstanta a $k$ je stupeň homogenity funkce.

Vyšetřeme nyní, co se přihodí, když vynásobíme všechny souřadnice číslem $\alpha$ a zároveň vynásobíme i čas dalším číslem $\beta$. Pro jednoduchost postupujme v kartézských souřadnicích:
\[ \vv r_a \to \alpha \vv r_a, \quad t \to \beta t. \]
Rychlosti jsou dány derivacemi $\D \vv r_a / \D t$, a proto se změní $\alpha / \beta$krát; kinetická energie se jakožto kvadratická jejich funkce změní $\alpha^2 / \beta^2$krát. Potenciální energie se podle našeho předpokladu násobí $\alpha^k$. Lagrangián tedy bude mít tvar
\[ \lagr = \frac{\alpha^2}{\beta^2} T - \alpha^k U, \]
z čehož vidíme, že aby se pohybové rovnice nezměnily, musí být oba činitele u $T$ i $U$ stejné; tedy má platit:
\[ \frac{\alpha^2}{\beta^2} = \alpha^k, \quad \text{tedy} \quad \beta = \alpha^{1 - k/2}. \]

Vynásobení všech souřadnic týmž číslem odpovídá přechodu od jedněch trajektorií k druhým, zcela podobným tvarem, pouze odlišným svými lineárními rozměry. Dosáhli jsme tedy závěru, že pokud je potenciální energie soustavy homogenní funkcí $k$-tého řádu, že připouštějí-li pohybové rovnice pohyb po nějaké trajektorii, připouštějí i pohyb po trajektorii geometricky podobné, přičemž čas pohybu (mezi odpovídajícími body na trajektoriích) se mění podle vztahu
\begin{equation} \label{10.2}
	\frac{t'}{t} = \left(\frac{l'}{l}\right)^{1 - k/2},
\end{equation}
kde $l'/l$ je poměr lineárních rozměrů těchto trajektorií. Úplně stejným způsobem zjistíme i změny dalších veličin v závislosti na $l'/l$ (neboť je možno je nakonec tak či onak rozepsat do závislosti na souřadnicích a čase). Pro rychlost, energii a moment hybnosti tedy dostaneme
\begin{align} \label{10.3}
	\frac{v'}{v} &= \left(\frac{l'}{l}\right)^{k/2},&
	\frac{E'}{E} &= \left(\frac{l'}{l}\right)^{k},&
	\frac{M'}{M} &= \left(\frac{l'}{l}\right)^{1 + k/2}.
\end{align}

Pro ilustraci poskytneme několik příkladů.

Jak uvidíme dále v této knize, v případě tak řečených malých kmitů je potenciální energie kvadratickou funkcí souřadnic (je tedy $k = 2$). Z \eqref{10.2} hned zjistíme, že perioda kmitů nemůže záviset na jejich amplitudě.

V homogenním silovém poli je podle \eqref{5.8} potenciální energie lineární funkcí souřadnic (tedy $k = 1$) Z \eqref{10.2} dostaneme
\[ \frac{t'}{t} = \sqrt{\frac{l'}{l}}. \]
Z toho je například vidět, že při volném pádu v homogenním tíhovém poli se k sobě čtverce dob pádu mají stejně jako počáteční výšky.

Při pohybu v Newtonově gravitačním poli nebo coulombovském poli elektrostatickém platí, že je potenciální energie nepřímo úměrná vzdálenosti mezi částicemi (je tedy homogenní funkcí řádu $-1$). V těchto případech tedy platí
\[ \frac{t'}{t} = \left(\frac{l'}{l}\right)^{\sfrac 3 2}, \]
z čehož můžeme dostat například tak zvaný \emph{třetí Keplerův zákon}, jenž říká, že se čtverce oběžných časů planet k sobě mají stejně jako třetí mocniny hlavních poloos jejich drah. {\footnotesize [ Čtenář nechť si ale všimne, že tento náš zákon je o dost obecnější ] }

Jestliže se mechanická soustava, v níž je potenciální energie homogenní funkcí souřadnic, pohybuje v omezené oblasti prostoru, existuje velmi jednoduchý vztah mezi časovými středními hodnotami kinetické a potenciální energie. Toto tvrzení je známo pod názvem \emph{viriálová věta}.

Jelikož je kinetická energie $T$ kvadratickou funkcí rychlostí, můžeme na ni použít Eulerovu větu o homogenních funkcích:
\[ \sum_a \pderiv{T}{\vv v_a} \vv v_a = 2T, \]
což je možno přepsat s pomocí hybností na
\begin{equation} \label{10.4}
	2T = \sum_a \vv p_a \vv v_a = \deriv{}{t} \left(\sum_a \vv p_a \vv r_a\right) - \sum_a \vv r_a \dot{\vv p_a}.
\end{equation}
Vypočteme z obou stran časovou střední hodnotu. Tak budeme nazývat veličinu
\[ \langle f \rangle = \lim_{\tau \to \infty} \frac{1}{\tau} \int\limits_0^\tau f(t) \D t. \]
Snadno uvidíme, že bude-li $f$ derivací od ohraničené funkce času (tedy od takové, která nemá nekonečné hodnoty) $F(t)$, bude její střední hodnota nulová. A skutečně,
\[ \langle f \rangle = \lim_{\tau \to \infty} \frac{1}{\tau} \int\limits_0^\tau \deriv F t \D t = \lim_{\tau \to \infty} \frac{F(\tau) - F(0)}{\tau} = 0. \]
{\footnotesize [ Čitatel zlomku v limitě je totiž podle našeho předpokladu určitě konečný, zatímco jmenovatel jde k nekonečnu. ] }

Předpokládejme, že se tedy soustava pohybuje v ohraničené oblasti prostoru a konečnými rychlostmi. Veličina $\sum \vv r_a \vv p_a$ je tedy ohraničená a střední hodnota prvního člene vpravo v rovnici \eqref{10.4} je rovna nule. Ve druhém podle Lagrangeových rovnic zaměníme $\dot{\vv p_a}$ za $-\pderiv{U}{\vv r_a}$ a dostaneme \footnote{Veličina na pravé straně rovnice \eqref{10.5} se označuje jako viriál soustavy. [ Název je odvozen od latinského \emph{vis}, síla, a pochází z pradávných dob, kdy se ještě energii říkalo \clqq živá síla\crqq, popřípadě \clqq mrtvá síla\crqq ]}:
\begin{equation} \label{10.5}
	2\langle T \rangle = \left\langle \sum_a \pderiv{U}{\vv r_a} \vv r_a \right\rangle.
\end{equation}
Je-li tedy potenciální energie homogenní funkcí $k$-tého řádu, jak jsme předpokládali, platí podle Eulerovy věty (viz \hyperref[6.3]{konec § 6}) vztah
\begin{equation} \label{10.6}
	2\langle T \rangle = k \langle U \rangle.
\end{equation}

Jelikož $\langle T \rangle + \langle U \rangle = \langle T + U \rangle = \langle E \rangle = E$, můžeme přepsat \eqref{10.6} do tvaru
\begin{align} \label{10.7}
	\langle U \rangle &= \frac{2}{k+2} E,&
	\langle T \rangle &= \frac{k}{k+2} E,
\end{align}
který umožňuje vyjádřit $\langle U \rangle$ a $\langle T \rangle$ pomocí celkové energie soustavy.

Speciálně pro malé kmity (s $k = 2$) platí
\[ \langle T \rangle = \langle U \rangle, \]
tedy střední hodnoty kinetické i potenciální energie jsou shodné. U newtonských interakcí ($k = -1$):
\[ 2\langle T \rangle = - \langle U \rangle. \]
Přitom je $E = - \langle T \rangle$ v souhlase s tím, že při těchto interakcích je ohraničený pohyb možný jen při záporné celkové energii (viz \hyperref[p15]{§ 15}).

{\footnotesize [ Kdyby se někomu zdálo, že viriálová věta nemá vzhledem k tomu, že potenciální energie je určena jen s přesností na aditivní konstantu, žádný smysl, pak se mýlí. Náš požadavek na homogenitu potenciální energie totiž tuto nepřesnost vylučuje. Za příklad může posloužit gravitační potenciál $-1/r$, který homogenní je, ale $-1/r + 1000$ už homogenní není. ] }

{\centering\bfseries\Large Cvičení

}

\small

\phantomsection \label{u10.1} \textbf{1.} Jak se liší doba pohybu po stejné trajektorii se stejnou potenciální energií, ale s různými hmotnostmi?

\textbf{Odpověď.}
\[ \frac{t'}{t} = \sqrt{\frac{m'}{m}}. \]

\phantomsection \label{u10.2} \textbf{2.} Jak se liší doba pohybu po stejné trajektorii při vynásobení potenciální energie nějakou konstantou?

\textbf{Odpověď.}
\[ \frac{t'}{t} = \sqrt{\frac{U'}{U}}. \]

