\section{Zobecněné souřadnice} \label{p1}

\setcounter{footnote}{0}

Jedním z důležitých konceptů v mechanice je pojem \emph{hmotného bodu} \footnote{Často budeme místo o \clqq hmotných bodech\crqq\ hovořit též o \clqq částicích\crqq.}. Tím myslíme těleso, jehož rozměry je možno při popisu jeho pohybu zanedbat. Samo sebou se rozumí, že přiměřenost takového zanedbání se mění od jedné úlohy k druhé --- planety je třeba možno považovat za hmotné body při popisu jejich obíhání kolem Slunce, ale rozhodně ne tehdy, když chceme popisovat jejich vlastní rotaci.

Umístění hmotného bodu v prostoru se určuje polohovým vektorem $\vv r$, jehož souřadnice odpovídají jeho kartézským souřadnicím $x, y, z$. Derivaci tohoto vektoru podle času $t$:
\[ \vv v = \deriv{\vv r}{t} \]
nazýváme rychlostí a druhou derivaci $\D^2 \vv r / \D t^2$ zrychlením hmotného bodu. Bývá také často běžné (a my se toho přidržíme) označovat derivaci podle času pomocí tečky nad písmenem, třeba takto: $\vv v = \dot{\vv r}$.

Pro popis nějaké soustavy $N$ hmotných bodů v prostoru je potřeba zadat $N$ takových polohových vektorů, tedy $3N$ souřadnic. Obecně se o počtu nezávislých veličin, které musí být zadány, aby byly polohy všech bodů soustavy jednoznačně zadány, jako o počtu \emph{stupňů volnosti} soustavy. (V případě soustavy $N$ bodů bez jakékoli vazby je to $3N$.) Tyto veličiny nemusí být rovny kartézským souřadnicím bodů v soustavě; k řešení některých úloh se hodí souřadnicové soustavy jiné {\footnotesize [ často takové, které respektují symetrii úlohy ]}. Libovolných $s$ veličin $q_1, q_2, \ldots, q_s$, které úplně charakterisují polohy všech bodů v soustavě s $s$ stupni volnosti, se nazývá jejími \emph{zobecněnými souřadnicemi}, a obdobně o derivacích $\dot q_i$ hovoříme jako o jejích \emph{zobecněných rychlostech}.

Zadání zobecněných souřadnic ovšem samo o sobě nestačí k úplnému určení \clqq mechanického stavu\crqq\ soustavy; tím míníme, že není možno z pouhých zobecněných souřadnic v jednom čase určit souřadnice bodů soustavy v některém jiném čase. Je to proto, že i při zadaných souřadnicích mohou mít jednotlivé body soustavy libovolné rychlosti, a právě na nich záleží, v jakém stavu bude soustava v následujícím okamžiku (tedy za nekonečně krátký čas $\D t$).

Zkušenost ukazuje, že současné zadání všech souřadnic a rychlostí už postačuje k tomu, abychom byli principiálně s to předurčit její další pohyb. Z matematického hlediska to začí, že zadáním všech poloh $q$ a rychlostí $\dot q$ v některý čas je v tomto čase jednoznačně určena i hodnota zrychlení $\ddot q$. \footnote{Pro stručnost budeme často pod symbolem $q$ rozumět obecně všechny zobecněné souřadnice $q_1, q_2, \ldots, q_s$, a podobně i pro rychlosti $\dot q$.}

Vztahy, které vážou zrychlení s rychlostmi a polohami, se nazývají \emph{pohybovými rovnicemi}. Vzhledem k souřadnicovým funkcím $q(t)$ to jsou diferenciální rovnice druhého řádu, jejichž integrací je možno v zásadě tyto funkce určit, a s nimi i trajektorie pohybu mechanické soustavy.

\section{Princip nejmenšího účinku} \label{p2}

Nejobecnější formulace pohybových zákonů pro mechanické soustavy je tak řečený \emph{Hamiltonův princip nejmenšího účinku} (též \emph{princip nejmenší akce}). Podle tohoto principu se každá mechanická soustava charakterisuje funkcí
\[ \lagr(q_1,q_2,\ldots,q_s,\dot q_1, \dot q_2, \dots, \dot q_s, t), \]
nebo též krátce $\lagr(q, \dot q, t)$, přičemž pohyb soustavy vyhovuje následující podmínce.

Nechť má soustava v časech $t_1$ a $t_2$ předem určené polohy zadané dvěma sadami zobecněných souřanic $q^{(1)}$ a $q^{(2)}$. Pak se mezi těmito polohami soustava pohybuje takovým způsobem, aby měl integrál
\begin{equation}\label{2.1}
	S = \int\limits_{t_1}^{t_2} \lagr(q, \dot q, t) \D t
\end{equation}
nejmenší možnou hodnotu. \footnote{Je potřeba dodat, že v této formulaci princip nejmenšího účinku neplatí pro všechny trajektorie po celé délce, ale jen pro každý z jejích dostatečně malých elementů. Dá se ukázat, že pro celou trajektorii musí mít integrál \eqref{2.1} jen extrémní, nikoli nutně minimální hodnotu. To ale nijak nepodkopává naše odvození pohybových rovnic, kde se stejně předpokládá jen extremalita řešení. {\footnotesize[ Ve skutečnosti pro některé soustavy (třeba pro harmonický oscilátor) má řečený integrál pouze \emph{stacionární} hodnotu, která není extrémní vůbec, trochu jako funkce $x^3$ má v nule stacionární bod (je tam nulová derivace), ale není tam ani minimum, ani maximum. Koho to zajímá víc, ať si najde nějakou knížku o variačním počtu a zaměří se v ní na pojem druhé variace. (Směstnávat látku na celou knížku do jedné poznámky pod čarou by bylo poněkud nemoudré.) ]}} Funkce $\lagr$ se nazývá \emph{Lagrangeovou funkcí} či též krátce \emph{lagrangiánem} dané soustavy a integrálu \eqref{2.1} se říká \emph{účinek}, nebo též \emph{akce}.

Fakt, že lagrangián závisí jen na $q$ a $\dot q$, ale nikoli na vyšších derivacích $\ddot q$ atd., je formálním vyjádřením tvrzení, že pohyb soustavy je úplně určen jen zadáním souřadnic a rychlostí.

Přejdeme nyní k odvození diferenciální rovnice, která řeší úlohu o minimalisaci integrálu \eqref{2.1}. Abychom se vyhnuli těžkopádnému označení, budeme předpokládat, že má soustava jen jeden stupeň volnosti a že je tudíž třeba určit jen jedinou funkci $q(t)$.

Řekněme, že $q(t)$ je právě ta funkce, pro kterou nabývá účinek $S$ minimální hodnoty, a podívejme se, co se přihodí s integrálem při záměně $q(t)$ za nějakou \clqq velmi blízkou\crqq\ funkci tvaru
\begin{equation}\label{2.2}
	q(t) + \delta q(t),
\end{equation}
kde $\delta q(t)$ je funkce, jež je v celém intervalu $\langle t_1; t_2\rangle$ malá proti $q(t)$ (říkají jí \emph{variace} funkce $q(t)$). V koncových bodech tohoto intervalu je ale poloha známa a dána pevně, takže musí platit
\begin{equation}\label{2.3}
	\delta q(t_1) = \delta q(t_2) = 0.
\end{equation}

Při záměně $q$ za $q + \delta q$ se $S$ změní o
\[ \int\limits_{t_1}^{t_2} \left[ \lagr(q + \delta q, \dot q + \delta \dot q, t) - \lagr(q, \dot q, t) \right] \D t. \]
V rozdílu za integrálem aproximujeme první lagrangián diferenciálem. Nutnou podmínkou pro minimalitu $S$ \footnote{Obecně --- extremalitu. [ A ještě obecněji --- stacionaritu. Viz ostatně mou poznámku k předchozí poznámce pod čarou. ]} je pak to, aby byl tento výraz (kterému se říká \emph{první variace} nebo často i prostě \emph{variace} $S$) roven pro všechna možná $\delta q$ identicky nule. [ Nabízí se analogie s obyčejnou derivací: v bezprostředním okolí extrému, kde je derivace nulová, je taky nulová první derivace, a tedy i diferenciál. ] Princip nejmenšího účinku je tedy možno zapsat rovnicí
\begin{equation}\label{2.4}
	\delta S = \delta \int\limits_{t_1}^{t_2} \lagr(q, \dot q, t) \D t = 0,
\end{equation}
nebo, pokud variaci provedeme,
\[ \int\limits_{t_1}^{t_2} \left(\lagr(q, \dot q, t) + \pderiv \lagr q \delta q + \pderiv{\lagr}{\dot q} \delta \dot q - \lagr(q, \dot q, t)\right) \D t = \int\limits_{t_1}^{t_2} \left(\pderiv \lagr q \delta q + \pderiv{\lagr}{\dot q} \delta \dot q \right) \D t. \]
Jelikož $\delta \dot q = \deriv{}{t} \delta q$, můžeme integrovat druhý člen per partes. Z toho dostaneme podmínku
\begin{equation}\label{2.5}
	\delta S = \left[ \pderiv{\lagr}{\dot q} \delta q \right]_{t_1}^{t_2} + \int\limits_{t_1}^{t_2} \left(\pderiv \lagr q - \deriv{}{t} \pderiv{L}{\dot q}\right) \delta q \D t = 0.
\end{equation}
Podle podmínky \eqref{2.3} ale první člen v tomto výrazu vypadne. Zůstane tedy integrál, který musí být sám o sobě roven nule pro libovolná $\delta q$, a to je možno jen tehdy, pokud je celá závorka identicky nulová.\footnote{[ Pokud vás tato argumentace nepřesvědčila, věřte, že se toto tvrzení dá i formálně dokázat na základě spojitosti $\lagr$. Dělá se to sporem tak, že se vezme hodnota $t$, pro kterou má závorka nenulovou hodnotu, díky spojitosti má závorka nenulovou hodnotu téhož znamení i v nějakém okolí tohoto bodu a zvolením $\delta q$ 1 na tomto okolí a 0 jinde dostaneme integrál různý od nuly, což je spor, protože jsme předpokládali, že ten integrál bude naopak nulový. Z toho plyne, že předpoklad o nenulovosti závorky neplatí a závorka je nulová. ]} Z toho dostaneme rovnici
\[ \deriv{}{t} \pderiv{\lagr}{\dot q} - \pderiv \lagr q = 0. \]

Pokud má soustava stupňů volnosti víc, budou se v principu nejmenšího účinku nezávisle na sobě variovat všechny funkce $q_i(t)$. Z toho zřejmě dostaneme $s$ rovnic ve tvaru
\begin{equation}\label{2.6}
	\deriv{}{t} \pderiv{\lagr}{\dot q_i} - \pderiv{\lagr}{q_i} = 0,\quad (i = 1, 2, \ldots, s).
\end{equation}
To jsou hledané diferenciální rovnice pro jednotlivé souřadnice; v mechanice se jim říká \emph{Lagrangeovy rovnice} \footnote{Ve variačním počtu, který se zabývá úlohami na extremalisaci takových a podobných integrálů obecně, se těmto rovnicím říká Eulerovy-Lagrangeovy.} Je-li tedy znám lagrangián dané mechanické soustavy, pak rovnice \eqref{2.6} stanoví vztahy mezi zrychleními, rychlostmi a souřadnicemi všech částic soustavy; jinými slovy, jde právě o její \emph{pohybové rovnice}.

Z pohledu matematika jsou rovnice \eqref{2.6} soustavou $s$ obyčejných diferenciálních rovnic druhého řádu pro $s$ neznámých funkcí $q_i(t)$. Obecné řešení takové soustavy bude obsahovat $2s$ konstant, které je třeba určit z téhož počtu počátečních podmínek. Abychom tedy určili, jaké pohyby bude soustava vykonávat, musíme nejdřív znát typicky všechny souřadnice a rychlosti v jednom okamžiku.

Mějme nyní soustavu, která sestává z dvou částí $A$ a $B$, které jsou uzavřené a které se navzájem neovlivňují (nebo u nichž můžeme alespoň vzájemné působení zanedbat). Mají-li tyto soustavy lagrangiány $\lagr_A$ a $\lagr_B$, pak platí pro lagrangián celé soustavy vztah
\begin{equation}\label{2.7}
	\lagr = \lagr_A + \lagr_B.
\end{equation}
Tato vlastnost popisuje fakt, že pohybové rovnice pro body v obou soustavách, které na sebe nijak nepůsobí, nemohou přirozeně obsahovat veličiny popisující body v jiných soustavách.

Z tvaru Lagrangeových rovnic by mělo být zřejmé, že vynásobení lagrangiánu libovolnou konstantou nijak tvar těchto rovnic nezmění. Při použití vlastnosti aditivity \eqref{2.7} je dokonce možno i násobit každý lagrangián jinou konstantou. {\footnotesize [ Autoři na tomhle místě sice tvrdí, že to přípustné není, ale kdo se podívá na Lagrangeovy rovnice \eqref{2.6}, tomu bude jasné, že není důvod, proč by to nemělo jít. Obávám se, že důvodem k tomu, aby to tvrdili, je snaha obejít problémy popsané v \hyperref[4.2]{překladatelské poznámce pod rovnicí 4.2}. ] } Takovéto násobení konstantou vlastně odpovídá výběru jednotek, v nichž budeme měřit fysikální veličiny. K tomuto tématu se ještě vrátíme v \hyperref[p4]{§ 4}.

Je ještě třeba připojit poznamenání o tak zvaných \emph{triviálních lagrangiánech}. To jsou hodnoty, které je možno přičíst k lagrangiánu, aniž se změní Lagrangeovy rovnice. V obecnosti je takovým triviálním lagrangiánem každá úplná časová derivace funkce souřadnic a času. Dokážeme to tím, že vyšetříme nový lagrangián tvaru
\begin{equation}\label{2.8}
	\lagr'(q, \dot q, t) = \lagr(q, \dot q, t) + \deriv{}{t} f(q, t).
\end{equation}
Pro tento lagrangián nyní vypočteme účinek podle \eqref{2.1} a dostaneme
\[ S' = \int\limits_{t_1}^{t_2} \lagr'(q, \dot q, t) \D t = \int\limits_{t_1}^{t_2} \left( \lagr(q, \dot q, t) + \deriv{}{t} f(q, t) \right) \D t = S + \left[f(q, t)\right]_{t_1}^{t_2}. \]
Nový lagrangián má tedy účinek rovný starému, jen k tomu přibyla jakási konstanta v hranatých závorkách. [ Konstanta to je, jelikož jde o rozdíl hodnot dané funkce v počátečním a koncovém času (a ty jsou známé a pevně dané). ] Při variování tyto konstanty tedy vypadnou a dostanou se úplně stejné Lagrangeovy rovnice jako předtím.

\section{Galileiho princip relativity} \label{p3}

Pro popis mechanických jevů si musíme vždy vybrat takovou nebo makovou \emph{vztažnou soustavu}. V různých vztažných soustavách mají zákony pohybu, obecně vzato, různý tvar. Pro řešení jistých úloh se některé vztažné soustavy hodí víc než jiné; ve většině z nich by byly pohybové rovnice zbytečně složité. Naším cílem bude přirozeně najít takovou vztažnou soustavu, ve které by pohybové rovnice měly tvar co možná nejjednodušší.

Kdybychom si zvolili vztažnou soustavu zcela libovolně, jevil by se prostor obecně nehomogenním [ neměl by ve všech bodech stejné vlastnosti ] a neisotropním [ neměl by ve všech směrech stejné vlastnosti ], a nadto by nemusel být homogenní ani čas, tj. jeho různé okamžiky by nebyly ekvivalentní. Že by takovéto vlastnosti času i prostoru do popisu mechanických dějů nepřinesly nic dobrého, je očividné --- třeba ani volná částice (tedy taková, na kterou nepůsobí žádné vnější vlivy) by nemohla být v klidu; i kdyby byla její rychlost v některém momentě nulová, hned v následujícím by se začala v některém směru pohybovat.

Ukazuje se ale, že je vždy možno najít takovou vztažnou soustavu, vzhledem ke které se prostor jeví homogenním a isotropním a čas homogenním. Taková soustava se nazývá \emph{inerciální}. V ní, mimo jiné, volná částice, která je v některý okamžik v klidu, zůstává v klidu neomezeně dlouho.

Můžeme hned stanovit některé vlastnosti lagrangiánu pro pohyb volné částice v inerciální vztažné soustavě. Homogenita prostoru i času znamená, že lagrangián nemůže záviset explicitně ani na polohovém vektoru $\vv r$, ani na čase $t$. Zbývá tedy závislost na rychlostech $\vv v$, ale kvůli isotropii prostoru nesmí záviset lagrangián ani na směrech těchto vektorů; musí být tedy funkcí pouze jejich velikostí, nebo též kvadrátů $\vv v^2$:
\begin{equation}\label{3.1}
	\lagr = \lagr(\vv v^2).
\end{equation}

Jelikož lagrangián na $\vv r$ vůbec nezávisí, platí $\partial \lagr / \partial \vv r = 0$ a Lagrangeovy rovnice mají tvar \footnote{Derivací skalární veličiny podle vektoru se rozumí takový vektor, v jehož složkách budou derivace onoho skaláru podle příslušných složek vektoru.}
\[ \deriv{}{t} \pderiv{\lagr}{\vv v} = 0, \]
odkud dostáváme $\partial \lagr / \partial \vv v = \text{const}$. Jelikož je ale tato parciální derivace pouze funkcí rychlosti, dostáváme i požadavek
\begin{equation}\label{3.2}
	\vv v = \text{const.}
\end{equation}

Takovým způsobem se tedy dostaneme k závěru, že v inerciální soustavě se volné částice pohybují s rychlostí konstantní velikosti i směru. Toto tvrzení je obsahem tak řečeného \emph{zákona setrvačnosti}.

\footnotesize
[ Jestliže se někomu nelíbí, že jsme nejdřív prohlásili lagrangián za nezávislý na směru a teď hovoříme o konstantním směru vektoru, ať zví, že to není nic proti ničemu. Při derivování totiž dostaneme
\[ \pderiv{\lagr(\vv v^2)}{\vv v} = \pderiv{\lagr}{(\vv v^2)} \cdot \deriv{\vv v^2}{\vv v} = \pderiv{\lagr}{(\vv v^2)} \cdot 2 \vv v. \]
Derivace tedy vytáhla na světlo i $\vv v$ jako takový (\clqq včetně směru\crqq). ]
\normalsize

Jestliže kromě naší inerciální soustavy zavedeme ještě soustavu jinou, která se vzhledem k první pohybuje rovnoměrně přímočaře, pak budou zákony volného pohybu vzhledem k této soustavě opět ve stejném tvaru jako prve: volný pohyb totiž bude opět probíhat s konstantní rychlostí.

Zkušenost ukazuje, že v těchto soustavách nebudou stejné jenom zákony volného pohybu, ale že jsou ekvivalentní i ve všech ostatních mechanických vlastnostech. Z tohoto důvodu neexistuje jen jedna inerciální soustava, ale je jich naopak nekonečné množství, z něhož každé dvě se navzájem vůči sobě pohybují rovnoměrně přímočaře. Ve všech těchto soustavách jsou všechny zákony mechaniky zcela stejné. Toto tvrzení se jmenuje \emph{Galileiho princip relativity} a jde o jeden z důležitých principů mechaniky.

Tím by tedy mělo být dost zřejmě vyloženo, jaké vlastnosti inerciální vztažné soustavy mají a proč se hodí při popisu mechanických jevů. Všude v dalším textu, pokud neřekneme jinak, budeme všechny pohyby vyšetřovat pouze v inerciálních soustavách.

Úplná mechanická ekvivalence všech nekonečně mnoha inerciálních soustav zároveň znamená, že mezi nimi neexistuje žádná \clqq absolutní\crqq\ vztažná soustava, které by bylo možno dávat přednost před všemi ostatními.

Souřadnice $\vv r$ a $\vv r'$ jednoho a téhož bodu ve dvou různých vztažných soustavách, z nichž se jedna pohybuje rychlostí $\vv V$ vůči druhé, jsou spolu vázány vztahem
\begin{equation} \label{3.3}
	\vv r = \vv r' + \vv V t.
\end{equation}
Při tom se čas v obou soustavách považuje za stejný:
\begin{equation} \label{3.4}
	t = t'.
\end{equation}
Předpoklad o absolutním plynutí času patří mezi základní představy \emph{klasické} mechaniky.

Vztahy \eqref{3.3}, \eqref{3.4} se nazývají \emph{Galileova transformace}.\footnote{V teorii relativity tato transformace neplatí vůbec. Místo ní se používá tak řečená transformace \emph{Lorentzova}.} Galileův princip relativity je možno také formulovat jako požadavek na invariantnost pohybových rovnic vůči této transformaci.

\section{Lagrangián volné částice} \label{p4}

Máme-li určit, jak vypadá lagrangián obecné mechanické soustavy, uděláme nejlépe, když začneme s něčím jednoduchým --- tedy pohybem volné částice v inerciální vztažné soustavě. Jak už jsme viděli, lagrangián takové soustavy může být pouze funkcí čtverce vektoru rychlosti. Abychom objasnili přesněji, jak tato závislost bude vypadat, použijeme Galileiho princip relativity. Jestliže se inerciální soustava $K$ pohybuje vzhledem k jiné inerciální soustavě $K'$ nekonečně malou rychlostí $\vv e$, pak je v této druhé soustavě rychlost částice $\vv v' = \vv v + \vv e$. Jelikož pohybové rovnice mají vypadat ve všech inerciálních soustavách stejně, musí se lagrangián $\lagr(\vv v^2)$ při přechodu do soustavy $K'$ změnit na funkci $\lagr'$, která se od původního $\lagr$ liší jen o úplnou časovou derivaci funkce souřadnic a času (viz \hyperref[2.8]{konec § 2}).

Máme tedy
\[ \lagr' = \lagr(\vv v'^2) = \lagr(\vv v^2 + 2\vv v \vv e + \vv e^2). \]
Rozloživše to v mocninnou řadu v $\vv e$ a zanedbavše nekonečně malé vyšších řádů, dostaneme
\[ \lagr(\vv v'^2) = \lagr(\vv v^2) + \pderiv{\lagr}{(\vv v^2)} \cdot 2\vv v \vv e. \]
Druhý sčítanec napravo bude úplnou časovou derivací funkce souřadnic a času pouze v případě, když bude na $\vv v$ záviset lineárně. { \footnotesize [ Je to tak proto, že úplná časová derivace funkce souřadnic a času vypadá následovně: $\D f(q, t) / \D t = \partial f / \partial t + \partial f / \partial q \cdot \dot q$. Sama $f$ na rychlostech nezávisí, takže jediné místo, kde se v tomto výrazu objevuje rychlost, je $\dot q$ úplně napravo. Z toho důvodu musí tedy $\D f / \D t$ záviset na $\vv v$ lineárně. ] } Proto $\partial \lagr / \partial (\vv v^2)$ už na rychlosti záviset nemůže a lagrangián musí být čtverci rychlosti přímo úměrný. Píšeme
\begin{equation} \label{4.1}
	\lagr = \half m \vv v^2,
\end{equation}
kde $m$ je konstanta. [ Proč jsme to tak napsali, uvidíme posléze. ]

Z toho, že lagrangián takového tvaru splňuje Galileův princip relativity pro nekonečně malou změnu rychlosti, vidíme už přímo i to, že bude Galileův princip splňovat i pro libovolné konečné rychlosti $\vv V$ mezi inerciálními soustavami $K$ a $K'$. A skutečně,
\[ \lagr' = \half m \vv v'^2 = \half m (\vv v + \vv V)^2 = \half m \vv v^2 + m \vv v \vv V + \half m \vv V^2, \]
nebo-li
\[ \lagr' = \lagr + \deriv{}{t} \left(m\vv r \vv V + \half m \vv V^2 t\right). \]
Jelikož je druhý člen vpravo úplnou časovou derivací funkce souřadnic a času, můžeme ho odstranit.

Veličina $m$ se nazývá \emph{hmotností} hmotného bodu. Díky aditivnosti lagrangiánů můžeme pro soustavu isolovaných bodů psát \footnote{Pro indexy, které probíhají přes všechny částice, budeme používat první písmena latinské abecedy; pro indexy probíhající souřadnicemi budeme používat písmena $i, j, k, \ldots$.}
\begin{equation} \label{4.2}
	\lagr = \sum_a \half m_a v_a^2.
\end{equation}

\footnotesize
[ Někde v těchto místech se nejspíš děje velice závažný a snad proto naprosto nekomentovaný myšlenkový skok, který si zasluhuje, aby se na něj aspoň upozornilo. Najednou se totiž začíná tiše předpokládat, že lagrangiány tohoto tvaru můžeme vesele sčítat, i když patří soustavám, které \emph{nejsou navzájem isolované}, ale které naopak sdílejí závislost na některých zobecněných souřadnicích. Třeba takové dvojité kyvadlo z \hyperref[u5.1]{úlohy 5.1} sestává ze dvou bodů, přičemž poloha spodního bodu závisí i na poloze bodu horního (a ta závisí na $\phi_1$). I přes to se jejich \clqq kinetické energie\crqq\ (jak se nazývají výrazy typu \eqref{4.2}) dají sčítat.

Pokud se toho ovšem využije, není už možné násobit každý z lagrangiánů libovolnou konstantou, nýbrž je dovoleno pouze násobit výsledný jejich součet jedním jediným číslem.

Nenapadá mě bohužel žádný rozumný způsob, jak to, co tu píšu, dokázat tak, abych nešel úplně proti duchu zbytku výkladu, a tak zatím musím nechat váženého čtenáře, aby tomu věřil --- nebo si někde našel odvození Lagrangeových rovnic z d'Alembertova principu a principu virtuální práce. ]
\normalsize

Musí se zdůraznit, že jen díky aditivitě mají hmotnosti nějaký skutečný fysikální smysl. Jak už jsme podotkli v \hyperref[p2]{§ 2}, je vždy možno vynásobit lagrangián libovolnou konstantou, aniž by se to odrazilo na pohybových rovnicích. Pro funkci tvaru \eqref{4.2} to vede pouze k výběru jednotky, pomocí které budeme hmotnosti měřit; poměry jednotlivých hmotností, které jediné mají fysikální smysl, při tom zůstanou vždycky zachovány.

Snadno uvidíme, že hmotnosti nemohou být záporné. Kdyby totiž byla hmotnost záporná, pak by integrál
\[ S = \int\limits_1^2 \half m v^2 \D t \]
neměl minimum; naopak tím, že bychom nejdříve hmotný bod hodně urychlili a těsně před koncovým bodem $2$ prudce zabrzdili, bychom mohli pro integrál dostat jakkoli velikou zápornou hodnotu. \footnote{To není proti upřesnění z první poznámky pod čarou v kapitole 2, protože účinek musí být minimální alespoň pro nekonečně malé elementy dráhy. Jelikož jsme ale naši úvahu provedli zcela nezávisle na délce dráhy, bude platit i pro takové elementy, a ani pro ně nebude mít účinek minimum.}

Ještě je vhodné dodat, že platí
\begin{equation} \label{4.3}
	v^2 = \left(\deriv l t\right)^2 = \frac{\D l^2}{\D t^2}.
\end{equation}
Pokud tedy známe element délky $\D l$ v souřadnicích, které pro řešení té které úlohy hodláme použít, můžeme při sestavování lagrangiánu tuto znalost výhodně zúročit.

Tak třeba v kartézských souřadnicích má element délky tvar $\D l^2 = \D x^2 + \D y^2 + \D z^2$, a lagrangián
\begin{equation} \label{4.4}
	\lagr = \half m \left(\dot x^2 + \dot y^2 + \dot z^2\right).
\end{equation}
Ve válcových je to $\D l^2 = \D r^2 + r^2 \D \phi^2 + \D z^2$, a lagrangián
\begin{equation} \label{4.5}
	\lagr = \half m \left(\dot r^2 + r^2 \dot \phi^2 + \dot z^2\right).
\end{equation}
Vposled ve sférických --- $\D l^2 = \D r^2 + r^2 \D \phi^2 + r^2 \sin^2 \phi \D \psi^2$, a tedy
\begin{equation} \label{4.6}
	\lagr = \half m \left(\dot r^2 + r^2 \dot \phi^2 + r^2 \sin^2 \phi \dot\psi^2\right).
\end{equation}

\section{Lagrangián soustavy hmotných bodů} \label{p5}

Nyní rozebereme pohyb soustavy hmotných bodů, které na sebe navzájem působí, ale na které nemají vliv žádná další tělesa mimo soustavu. Takovým soustavám se říká \emph{uzavřené}. Ukazuje se, že interakce mezi částicemi se dají popsat tak, že se k lagrangiánu navzájem neinteragujících částic \eqref{4.2} připočte nějaká funkce souřadnic (jejíž přesný tvar závisí na charakteru interakcí). \footnote{To je pravda pouze v klasické mechanice, která se vykládá v této knížce. Mechanika relativistická takové věci nedovoluje.} Označíme tuto funkci jako $-U$ a napíšeme
\begin{equation} \label{5.1}
	\lagr = \sum_a \half m_a v_a^2 - U(\vv r_1, \vv r_2, \ldots),
\end{equation}
kde $\vv r_a$ je polohový vektor $a$-tého bodu. To je tedy obecný tvar lagrangiánu pro uzavřenou soustavu.

Součet
\[ T = \sum_a \half m_a v_a^2 \]
se nazývá \emph{kinetickou energií} soustavy a funkce $U$ její \emph{potenciální energií}. Smysl těchto názvů se objasní v \hyperref[p6]{§ 6}.

Fakt, že potenciální energie závisí jen na rozpoložení hmotných bodů v jednom jediném čase, ukazuje, že se změna polohy libovolné z nich okamžitě odrazí na všech ostatních. Můžeme tedy říkat, že se interakce \clqq šíří nekonečně rychle\crqq. Že mají interakce právě takové vlastnosti, je těsně svázáno s absolutností času a Galileiho principem relativity. Kdyby se totiž interakce šířily nějakou konečnou rychlostí, pak by tato rychlost musela být různá v různých inerciálních soustavách (které se navzájem vůči sobě pohybují nějakou rychlostí), protože absolutnost času dovoluje používat na všechny jevy obyčejný zákon skládání rychlostí. Pak by ale pohybové rovnice interagujících částic byly v různých inerciálních soustavách různé, a to by protiřečilo principu relativity.

V \hyperref[p3]{§ 3} jsme hovořili pouze o homogenitě času. Tvar lagrangiánu \eqref{5.1} ovšem ukazuje, že je čas i isotropní, tedy že má stejné vlastnosti v obou směrech; přesněji řečeno, že záměna $t$ za $-t$ lagrangián, a tedy ani pohybové rovnice, nijak nezmění. Jinak řečeno: je-li v nějaké soustavě možný jistý pohyb, pak je vždy možný i pohyb opačný, tedy takový, při kterém soustava prochází týmiž stavy, pouze v opačném pořádku. Díky tomu jsou tedy všechny pohyby, které se v klasické mechanice dějí, vratné.

Když tedy známe lagrangián, můžeme sestavit pohybové rovnice:
\begin{equation}\label{5.2}
	\deriv{}{t} \pderiv{\lagr}{\vv v_a} = \pderiv{\lagr}{\vv r_a}.
\end{equation}
Po dosazení \eqref{5.1} se dopracujeme k
\begin{equation}\label{5.3}
	m_a \deriv{\vv v_a}{t} = - \pderiv{U}{\vv r_a}.
\end{equation}
Pohybové rovnice tohoto tvaru se nazývají \emph{Newtonovými rovnicemi}. To jsou základní rovnice mechaniky pro pohyb vzájemně interagujících částic. Vektor
\begin{equation}\label{5.4}
	\vv F_a = - \pderiv{U}{\vv r_a},
\end{equation}
který se nalézá na pravé straně rovnice \eqref{5.3}, se jmenuje \emph{síla} působící na $a$-tý bod soustavy. Stejně jako $U$ závisí i ona jen na souřadnicích všech částic, ale ne na jejich rychlostech. Rovnice \eqref{5.3} pak ukazují, že i vektory zrychlení jsou opět jen funkcemi souřadnic.

Potenciální energie je veličina, která je určena jen s přesností na aditivní konstantu. To znamená, že pokud k ní přičteme konstantu, pak se pohybové rovnice nijak nezmění (to totiž vyústí jen v přidání konstanty k lagrangiánu a přitom, jak víme z \hyperref[2.7]{§ 2}, zůstanou pohybové rovnice stejné). Často se tato konstanta vybírá tak, aby potenciální energie konvergovala k nule, pokud jde vzdálenost mezi částicemi k nekonečnu {\footnotesize [ ale stejně tak často se vybírá i úplně jinak $\ddot\smile$ ]}.

Pokud jsme pro popis pohybu použili jiné než kartézské souřadnice, pak musíme provést příslušný převod mezi našimi zobecněnými a kartézskými souřadnicemi:
\[ x_a = f_a(q_1, q_2, \ldots, q_s), \quad \dot x_a = \sum_k \pderiv{f_a}{q_k} \dot q_k \text{atd.} \]
Když to dosadíme do lagrangiánu v kartézských souřadnicích
\[ \lagr = \half \sum_a m_a(\dot x_a^2 + \dot y_a^2 + \dot z_a^2) - U, \]
dostaneme ve skutečnosti funkci tvaru
\begin{equation} \label{5.5}
	\lagr = \half \sum_{i, k} a_{ik}(q) \dot q_i \dot q_k - U(q),
\end{equation}
kde $a_{ik}$ jsou funkce souřadnic. Kinetická energie v zobecněných souřadnicích je tedy kvadratickou funkcí rychlostí, ale může záviset i na souřadnicích.

Zatím jsme hovořili jen o uzavřených soustavách. Nyní rozeberme pohyb soustavy $A$, na kterou působí jiná soustava $B$. V tomto případě se mluví o tom, že se soustava $A$ pohybuje v zadaném vnějším poli (určeném soustavou $B$). Protože se pohybové rovnice dostanou z principu nejmenšího účinku nezávislou variací každé souřadnice zvlášť, tedy tak, jako by byly všechny ostatní známy, můžeme pro nalezení lagrangiánu $\lagr_A$ funkce A použít lagrangián soustavy $A+B$, přičemž v ní souřadnice $q_B$ zaměníme za \clqq známé\crqq\ funkce času.

Předpokládejme, že $A+B$ je již soustava uzavřená, a dostaneme:
\[ \lagr = T_A(q_A, \dot q_A) + T_B(q_B, \dot q_B) - U(q_A, q_B), \]
kde první dva členy představují úhrnné kinetické energie soustav $A$ a $B$ a třetí potenciální energii jejich vzájemného působení. Když nahradíme $q_B$ zadanou funkcí času, zjistíme, že člen $T(q_B(t), \dot q_B(t))$ závisí pouze na čase a tudíž musí být úplnou časovou derivací nějaké jiné funkce, která závisí pouze na čase. Díky tomu se tohoto členu zbavíme a zůstane
\[ \lagr_A = T_A(q_A, \dot q_A) - U(q_A, q_B(t)). \]

Došli jsme tedy jen k tomu závěru, že pohyb ve vnějším poli se popisuje lagrangiánem úplně stejného tvaru jako jsou ty, s nimiž jsme nakládali doposud, jen s tím, že potenciální energie může explicitně záviset na čase.

Pohyb jedné částice ve vnějším poli je tedy dán lagrangiánem obecného tvaru
\begin{equation} \label{5.6}
	\lagr = \half mv^2 - U(\vv r, t)
\end{equation}
a pohybové rovnice
\begin{equation} \label{5.7}
	m\dot{\vv v} = - \pderiv{U}{\vv r}.
\end{equation}

Pole, ve kterém na částici působí v každém bodě stejná síla $\vv F$, se nazývá \emph{homogenní} a jeho potenciální energie je zřejmě rovna
\begin{equation} \label{5.8}
	U = - \vv F \vv r.
\end{equation}

Na závěr tohoto paragrafu ještě poznamenáme něco k použití Lagrangeových rovnic na řešení konkrétních úloh. Často se musíme obírat soustavami, v nichž mají interakce mezi částicemi či tělesy charakter \emph{vazeb}, tedy podmínek, které klademe na jejich vzájemné polohy. Fakticky se takové vazby zařizují spojováním těles nejrůznějšími tyčkami, provázky, pružinami atd. Kvůli všem těmto věcem se v pohybu objevuje nový faktor, a to tření v místech, kde jsou k sobě tělesa přidělána. Řešení takových úloh ovšem již překračuje, obecně řečeno, rámec čisté mechaniky (ale viz \hyperref[p25]{§ 25}). Naštěstí je však v mnoha případech tření tak slabé, že se dá zcela bezpečně zanedbat. Pokud je k tomu možno zanedbat i hmotnosti tyček aj., kterými se vazby realisují, je jediným výsledkem zavedení takových vazeb snížení počtu stupňů volnosti. K stanovení jejího pohybu je možno znova využít lagrangián ve tvaru \eqref{5.5} s takovým počtem nezávislých zobecněných souřadnic, který odpovídá skutečnému počtu stupňů volnosti soustavy.

{\centering\bfseries\Large Cvičení

}

\small

Najít lagrangiány následujících soustav, jestliže se nalézají v homogenním gravitačním poli (přičemž zrychlení tíhové síly je všude $g$).

\rightfig 1

\phantomsection \label{u5.1} \textbf{1.} Dvojité rovinné kyvadlo (obr. 1).

\textbf{Řešení}. Za zobecněné souřadnice vezmeme úhly $\phi_1, \phi_2$, které svírají tyčky $l_1, l_2$ se svislicí. Pro bod $m_1$ pak dostaneme
\begin{align*}
	T_1 &= \half m_1 l_1^2 \dot \phi_1^2, & U &= - m_1gl_1 \cos \phi_1.
\end{align*}
Abychom našli kinetickou energii druhého bodu, vyjádříme její kartézské souřadnice (s počátkem v závěsu celého kyvadla a s osou $y$ směřující svisle dolů) pomocí úhlů $\phi_1, \phi_2$:
\begin{align*}
	x_1 &= l_1 \sin \phi_1 + l_2 \sin \phi_2, & y_2 &= l_1 \cos \phi_1 + l_2 \cos \phi_2.
\end{align*}

Z toho dostaneme
\[ T_2 = \half m_2 (\dot x_2^2 + \dot y_2^2) = \half m_2 [ l_1^2 \dot \phi_1^2 + l_2^2 \dot \phi_2^2 + 2l_1l_2 \cos(\phi_1 - \phi_2) \dot \phi_1 \dot \phi_2 ]. \]
Nakonec
\[ \lagr = \frac{m_1 + m_2}{2} l_1^2 \dot \phi_1^2 + \half m_2 l_2^2 \dot \phi_2^2 + m_2 l_1l_2 \dot \phi_1 \dot \phi_2 \cos(\phi_1 - \phi_2) + (m_1+m_2) gl_1 \cos \phi_1 + m_2 gl_2 \cos \phi_2. \]

\fig{2}

\phantomsection \label{u5.2} \textbf{2.} Rovinné kyvadlo s hmotností $m_2$, jehož závěs (zatížený hmotností $m_1$) se může pohybovat po vodorovné přímce (obr. 2).

\textbf{Řešení.} Zavedeme souřadnici $x$ závěsu na přímce a úhel $\phi$ mezi tyčkou kyvadla a vertikálou, načež obdržíme
\[ \lagr = \frac{m_1 + m_2}{2} \dot x^2 + \half m_2 (l^2 \dot\phi^2 + 2l\dot x \dot \phi \cos \phi) + m_2 gl \cos \phi. \]

\rightfig 3

\phantomsection \label{u5.3} \textbf{3.} Rovinné kyvadlo, jehož závěs:
\begin{ce}
\item se rovnoměrně pohybuje po svisle umístěné kružnici s konstantní frekvencí $\gamma$ (obr. 3);
\item harmonicky kmitá na vodorovné přímce (podle vztahu $x = a \cos \gamma t$);
\item harmonicky kmitá na svislé přímce (podle vztahu $y = a \cos \gamma t$).
\end{ce}

\textbf{Řešení.} 1: Souřadnice bodu $m$ na kyvadle:
\begin{align*}
	x &= a \cos \gamma t + l \sin \phi, & y &= -a \sin \gamma t + l \cos \phi.
\end{align*}
Lagrangián je tedy
\[ \lagr = \half ml^2 \dot\phi^2 + mla\gamma^2 \sin(\phi - \gamma t) + mgl \cos\phi; \]
při tom jsme odstranili členy závislé pouze na čase a též jsme se zbavili úplné časové derivavce z $mal\gamma \cos(\phi - \gamma t)$.

2: Souřadnice bodu $m$:
\begin{align*}
	x &= a \cos \gamma t + l \sin \phi, & y &= l \cos \phi.
\end{align*}
Lagrangián (po zneškodnění úplných časových derivací):
\[ \lagr = \half ml^2 \dot\phi^2 + mla\gamma^2 \cos \gamma t \sin \phi + mgl \cos \phi. \]

3: Zcela analogicky:
\[ \lagr = \half ml^2 \dot\phi^2 + mla\gamma^2 \cos \gamma t \cos \phi + mgl \cos \phi. \]

\fig 4

\phantomsection \label{u5.4} \textbf{4.} Soustava na obr. 4. Spodní bod $m_2$ se pohybuje po svislici [ v bodech $m_1$ vlevo a vpravo se přitom mění úhel mezi tyčkami ] a kromě toho ještě celá soustava rotuje konstantní úhlovou rychlostí $\Omega$ kolem svislé osy.

\textbf{Řešení.} Zavedeme úhel $\theta$ mezi svislicí a některou z tyček, jak je to ukázáno na obrázku, a také úhel $\phi$ otočení celé soustavy kolem její osy rotace. Platí tedy $\dot\phi = \Omega$. Pro oba body $m_1$ je element posunu roven $\D l_1^2 = a^2 \D \theta^2 + a^2 \sin^2 \theta \D \phi^2$. Bod $m_2$ je od závěsu $A$ vzdálen $2a\cos\theta$, a proto $\D l_2 = -2a \sin\theta \D \theta$. Lagrangián je tedy
\[ \lagr = m_1a^2(\dot\theta^2 + \Omega^2 \sin^2\theta) + 2m_2a^2 \dot \theta^2 \sin^2 \theta + 2ga(m_1 + m_2) \cos \theta. \]

\normalsize
